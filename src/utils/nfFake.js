const branches = [
  'Medina', 'Adenta', 'Kasoa', 'Labadi', 'labone', 'Ashaley Botwe',
  'Edwisu', 'Suotwom', 'Mallam', 'McCarthy Hill', 'Darkuman',
  'Dansoman'
]
export default {
  branch () {
    return branches[Math.floor(Math.random() * branches.length)] + ' Branch'
  },
  district () {
    return branches[Math.floor(Math.random() * branches.length)] + ' District'
  }
}
