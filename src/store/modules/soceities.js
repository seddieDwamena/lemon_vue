import sectorsRepo from '../../repo/clientsRepo'
import {ASCENDING, CURRENT_URL} from '../../utils/Constants.js'
import Utils from '../../utils/Utils.js'
import axios from 'axios'
import store from '@/store/index'
import * as types from '@/store/mutation-types'
import {db} from '@/services/LocalStorage'

// initial state
const state = {
  soceities: {
    data: db.get('SOCIETIES', []),
    originalData: null,
    meta: {page: 1},
    errors: [],
    state: 'DATA',
    filters: [],
    sortParams: {param: null, order: ASCENDING},
    soceitiesCount: 0
  },
  currentsoceity: null
}

// getters
const getters = {
  soceities: state => state.soceities.data,
  soceitiesCount: state => state.soceities.data.length,
  // for the viewstate to always show the right view always set the state before the data
  // the viewstate is slightly different from the state
  // the view state is specifically used by the view to render the appropriate screen
  // for eg. a state of loading will have a viewstate of data if there are soceities while  
  // they are loading
  soceitiesViewState: state => {
    let soceitiesSubModule = state.soceities
    let hasAgents = soceitiesSubModule.data && soceitiesSubModule.data.length > 0
    if (hasAgents) {
      return 'DATA'
    }
    if (soceitiesSubModule.state !== 'LOADING' && soceitiesSubModule.state !== 'ERROR') {
      return 'NO-DATA'
    }
    return soceitiesSubModule.state
  },
  soceitiesMeta: state => state.soceities.meta
}

// actions
const actions = {
  getSoceities ({ commit, state, rootGetters }) {
    let client_code = rootGetters.user.client_code
    const URL = `${CURRENT_URL}societies?client_codes[]=${client_code}&limit=10&page=1`
    axios.get(
      URL
    ).then(r => r)
    .then((data) => {
      commit('SET_SOCEITIES', data)
    })
  }
}

// mutations
const mutations = {
  SET_SOCEITIES (state, soceities) {
    state.soceities.data = soceities.societies
    db.set('SOCIETIES', soceities.societies)
    state.soceitiesCount = soceities.societies.length
  },
  ADD_SOCIETY (state, society) {
    state.soceities.data = state.soceities.data || []
    state.soceities.data.unshift(society)
    db.set('SOCIETIES', state.soceities)
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
