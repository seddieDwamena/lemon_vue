import Repository from '../services/Repository.js'
export default{
  singleContact () {
    return Repository.fetchContacts()[0]
  },
  allContacts () {
    return Repository.fetchContacts()
  }
}
