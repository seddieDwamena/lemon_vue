import axios from 'axios';
import {API_URL, FAKE_API_URL, BEIGE_BACKEND, BEIGE_BACKEND_ANDY, CURRENT_URL} from '../utils/Constants.js'
import qs from 'qs'
const USE_FAKE_API = true

export default {
  get (params) {
    // const URL = USE_FAKE_API ? `${FAKE_API_URL}agents` : `${API_URL}agents`
    // params.Page = params.Page || 1
    // params.PageSize = params.PageSize || 30
    // params.state = params.state || "Active"

    // return axios.get(URL, {params: params,
    //   paramsSerializer: function(params) {
    //     return qs.stringify(params, {arrayFormat: 'repeat'})
    //   },
    // })
    // .then(data => {
    //   let agents = data.agents.map( el => {
    //     el.branch = el.branch_assigned
    //     el.type = el.is_own_agent && !el.is_super_agent ? "Field Agent" : "Third Party"
    //     return el
    //   })
    //   return {
    //     agents: agents,
    //     meta: {Page: data.current_page, PageSize: data.current_page_total,
    //       TotalPages: data.total_pages, TotalFilteredRecords: data.total_filtered,
    //       TotalRecords: data.total_records
    //     }
    //   }
    // })

    const URL = `${CURRENT_URL}stocks`

    params.page = params.Page || 1
    params.limit = params.PageSize || 30
    // params.state = params.state || "Active"

    return axios.get(URL, {params: params,
      paramsSerializer: (params) => {
        return qs.stringify(params, {arrayFormat: 'repeat'})
      }
    })
    .then(data => {
      let stock = data.stocks.map(el => {
        el.name = `${el.first_name} ${el.last_name}`
        return el
      })
      return {
        stock: stock,
        meta: {Page: data.current_page,
          PageSize: data.current_page_total,
          TotalPages: data.total_pages,
          TotalFilteredRecords: data.total_filtered,
          TotalRecords: data.total_records
        }
      }
    })
  }

}
