import Vue from 'vue'
import notify from '@/utils/notify';
import Router from 'vue-router'
import Clients from '@/pages/Clients'
import Contacts from '@/pages/client/Contacts'
import ContactShow from '@/pages/client/contacts/ContactShow'
import Agents from '@/pages/client/Agents'
import AgentShow from '@/pages/client/agents/AgentShow'
import AgentShowMap from '@/pages/client/agents/transactions/showMap'
import Show from '@/pages/client/Show'
import AdminSettings from '@/pages/client/AdminSettings'
import AdminSettingsShow from '@/pages/client/adminsettings/AdminSettingsShow'
import AdminSettingsUserFields from '@/pages/client/adminsettings/AdminSettingsUserFields'
import InProgress from '@/pages/InProgress'
import AdminSettingsUser from '@/pages/client/adminsettings/users/AdminSettingsUser'
import LoanApplications from '@/pages/client/adminsettings/LoanApplications'
import Progress from '@/pages/InProgress'
import Transactions from '@/pages/client/Transactions'
import StockTransactions from '@/pages/client/StockTransactions'
import Dashboard from '@/pages/client/Dashboard'
import SelfService from '@/pages/client/SelfService'
import PostTransaction from '@/pages/client/PostTransaction'
import AdminSectorShow  from '@/pages/client/adminsettings/sectors/AdminSectorShow'
import AdminDistrictShow  from '@/pages/client/adminsettings/AdminDistrictShow'
import AdminSoceityShow  from '@/pages/client/adminsettings/AdminSoceityShow'
import AdminTownShow  from '@/pages/client/adminsettings/AdminTownShow'
import Help from '@/pages/client/Help'
import Login from '@/pages/gsma/Login'
import Reset from '@/pages/Reset'
Vue.use(Router)
import auth from '@/services/auth'
import store from '@/store'
import axios from 'axios'
import {CURRENT_URL, ngrok_url} from '../utils/Constants.js'

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'clients',
      redirect: '/dashboard',
      component: Clients,
      children: [
        {
          path: 'post-transactions',
          component: PostTransaction,
          name: 'post-transactions'
        },
        {
          path: 'contacts',
          component: Contacts,
          name: 'contacts'
        },
        {
          path: 'help',
          component: Help,
          name: 'help'
        },
        {
          path: 'contacts/:id',
          component: ContactShow,
          name: 'contact',
          props: true,
          meta: {hasInitialData: true}
        },
        {
          path: 'agents',
          component: Agents,
          name: 'agents'
        },
        {
          path: 'agents/:id',
          component: AgentShow,
          name: 'agent',
          props: true
        },
        {
          path: 'show',
          component: Show,
          name: 'show'
        },
        {
          path: 'adminsettings',
          component: AdminSettings,
          name: 'adminsettings'
        },
        {
          path: 'adminsettings/users',
          component: AdminSettingsShow,
          name: 'adminsetting',
          props: true
        },
        {
          path: 'adminsettings/sectors',
          component: AdminSectorShow,
          name: 'adminsector',
          props: true
        },
        {
          path: 'adminsettings/districts',
          component: AdminDistrictShow,
          name: 'admindistrict',
          props: true
        },
        {
          path: 'adminsettings/soceities',
          component: AdminSoceityShow,
          name: 'adminsoceity',
          props: true
        },
        {
          path: 'adminsettings/towns',
          component: AdminTownShow,
          name: 'admintown',
          props: true
        },
        {
          path: 'adminsettings/users_fields',
          component: AdminSettingsUserFields,
          name: 'adminsettingsUserFields'
        },
        {
          path: 'adminsettings/users/:id',
          component: AdminSettingsUser,
          name: 'admin_settings_user',
          props: true
        },
        {
          path: 'originate/loan_applications',
          component: LoanApplications,
          name: 'loan_applications'
        },
        {
          path: 'transactions',
          component: Transactions,
          name: 'transactions'
        },
        {
          path: 'stock',
          component: StockTransactions,
          name: 'stock'
        },
        {
          path: 'dashboard',
          component: Dashboard,
          name: 'dashboard'
        },
        {
          path: 'selfservice',
          component: SelfService,
          name: 'selfservice'
        },
      ],
    },
    {
      path: '/prog',
      component: Progress,
      name: 'prog'
    },
    {
      path: '/login',
      component: Login,
      name: 'login'
    },
    {
      path: '/reset-password',
      component: Reset,
      name: 'resetPassword'
    }
  ]
})

let headersSet = false

router.beforeEach((to, from, next) => {
  // this.$store.commit('setToken', null)
  //  this.$store.commit('setClient', null)
  // next()
  // return
  // console.info('logged?', auth.loggedIn())
  // if(to.name == 'resetPassword'){
  //   if(auth.loggedIn()){
  //     next()
  //   }else{
  //     next({
  //       name: 'login',
  //       query: { redirect: to.name }
  //     })
  //   }
  // }
  // if(auth.first_time_login){
  //   // alert()
  //   next({
  //     name: 'resetPassword',
  //     query: { redirect: to.name }
  //   })
  //   return
  // }
  // alert(`${to.name} ${from.name}`)
  if (to.name === 'login') {
    if (auth.loggedIn()) {
      next(from.path)
    } else {
      next()
    }
  } else if (to.name === 'resetPassword') {
    // alert()
    if (!auth.loggedIn()) {
      next({
        path: '/login',
        query: { redirect: to.name }
      })
    } else {
      next()
    }
  } else if (auth.first_time_login) {
    next({
      name: 'resetPassword',
      query: { redirect: to.name }
    })
  } else {
    if (!auth.loggedIn()) {
      next({
        path: '/login',
        query: { redirect: to.name }
      })
    } else {
      if (!headersSet) {
        axios.defaults.headers.common['Authorization'] = `Bearer ${store.getters.token}`
        axios.defaults.headers.common['Login-Type'] = `client`

        // these should be fetched before showing the app 
        // fetch agents for dashoard todo move this to later
        console.info('i coming')
        axios.get(`${CURRENT_URL}agents`)
        .then(data => {
          store.commit('SET_AGENT_MAP_DATA', data.agents)
          return true
        })
        .then(() => {
          return axios.get(`${CURRENT_URL}users`)
        })
        .then(data => {
          store.commit('SET_USER_MAP_DATA', data.users)
          return true
        })
        // fetch user data
        .then(() => axios.get(`${CURRENT_URL}user_data`))
        .then(data => {
          store.commit('setUser', data)
          return true
        })
        // fetch regions
        .then(() => {
          return store.dispatch('fetchUserData')
        })
        .then(() => {
          let matchedComponent = to.matched.find(el => el.meta.hasInitialData === true)
          console.info('math')
           // if component has initialData it will load first before setting this to loaded
          if (matchedComponent) {
            console.info(matchedComponent.components.default.methods)
            return matchedComponent.components.default.methods.fetchInitialData({store, route: to})
          } else {
            return Promise.resolve(true)
          }
        })
        .then(() => {
          headersSet = true
          store.commit('setInitialDataState', 'LOADED')
        }, error => {
          console.info(error)
          store.commit('setInitialDataState', 'ERROR')
        })
        // these are fetched afterwards
        .finally(() => {
          store.dispatch('getSoceities')
          store.dispatch('getDistricts')
          store.dispatch('getTowns')
          store.dispatch('getSectors')
          store.dispatch('getGroups')
        })
        next()
      } else {
        // console.info('route', to, from)
        let route = auth.getAuthenticatedRoute({to, from})
        if (to.name === route) {
          next()
        } else {
          notify.info('You do not have permissions to access this page')
          next({name: route})
        }
      }
    }
  }
})

export default router
