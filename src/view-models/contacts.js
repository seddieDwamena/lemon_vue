import Utils from '../utils/Utils.js'

// properties
let ContactsViewModel = function () {

}

// methods
ContactsViewModel.prototype.getForm = function (contact) {
  if (!contact) {
    return {
      service_ids: [],
      first_name: '',
      surname: '',
      other_names: '',
      dob: '',
      place_of_birth: '',
      code: '',
      type: null,
      supplier: 13,
      town_id: null,
      region_id: null,
      // region_id: null,
      active: true,
      addresses: [
        {
          house_number: '',
          street: '',
          description: '',
          city: '',
          suburb: '',
          post_code: ''
        }
      ],
      phone_numbers: [
        {
          carrier: null,
          primary: true,
          ported: false,
          number: ''
        }
      ]
    }
  }

  let form = Utils.cloneObject(contact)
  // form.type = contact.contact_type 
  form.phone_numbers = [contact.registered_phone]
  if (!contact.registered_phone) {
    form.phone_numbers = [{
      carrier: '',
      primary: true,
      ported: false,
      number: ''
    }]
  }
  if ((!form.addresses && form.addresses[0])){
    form.addresses = [{
      house_number: '',
      street: '',
      description: '',
      city: '',
      suburb: '',
      post_code: ''
    }]
  }
  form.addresses = form.addresses.map(el => {
    return {
      house_number: el.house_number,
      street: el.street,
      description: el.description,
      city: el.city,
      suburb: el.suburb,
      post_code: el.post_code
    }
  })
}

let contactViewModel = new ContactsViewModel()
export {contactViewModel}
