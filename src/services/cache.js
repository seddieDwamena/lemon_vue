import store from '@/store/index'
import axios from 'axios'

let Cache = function (obj) {
  this.bucket = {}
}

// methods
Cache.prototype.generateKey = function (key, id) {
  let mKey = key
  if (id) {
    mKey = `key_${id}`
  }
  return mKey
}

Cache.prototype.set = function (key, value, id) {
  let mKey = this.generateKey(key, id)
  this.bucket[mKey] = {value, created: Date.now()}
}

Cache.prototype.get = function (key, id) {
  let mKey = this.generateKey(key, id)
  let value = this.bucket[mKey]
  if (value) {
    return value.value
  }
  return false
}

const cache = new Cache()

let refreshInterval = 60 * 60 * 40

export default cache
