import agentsApi from '../api/agentsApi.js'

export default {
  get (params) {
    return agentsApi.get(params)
  },
  find (id) {
    return agentsApi.find(id) 
  }
}
