import {ASCENDING, DESCENDING, FIRSTNAME, SURNAME} from './Constants.js'
export default {
  longestString (arr) {
    (accumulator, currentValue) => accumulator + currentValue
    return arr.reduce((a, b) => a.length > b.length ? a : b )
  },

  // performAnimation (animationFunction) {
  //
  // },
  approvalStatusName (status) {
    status = status ? status.toLowerCase() : ''
    switch (status) {
      case 'unapproved':
        return 'Pending Approval'
        break
      case 'approved':
        return 'Active'
        break
      case 'rejected':
        return 'Rejected'
        break
      default:
        return ''
    }
  },
  realWidth (obj, mountObj) {
    let clone = obj.clone()
    console.log('clone', clone)
    clone.css('visibility', 'hidden').css('display', 'inline')
    mountObj.append(clone)
    let width = clone.outerWidth()
    console.log(width)
    clone.remove()
    return width
  },
  // returns a random boolean biased on a scale 1 to 10
  randomBoolean (bias) {
    let randomNumber = Math.floor(Math.random() * 10)
    if (bias) {
      return randomNumber < bias
    } else {
      return randomNumber < 5
    }
  },
  concatString (stringArray, separator) {
    return stringArray.reduce((acc, el) => {
      return `${acc && acc !== '' ? acc + separator : ''}${el ? el : ''}`
    }, null)
  },
  cloneObject (obj) {
    return JSON.parse(JSON.stringify(obj))
  },
  swapArrayElements (a, x, y) {
    if (a.length === 1) return a
    a.splice(y, 1, a.splice(x, 1, a[y])[0])
    return a
  },
  // removes object members with empty values i.e null, []
  removeNulls (obj) {
    let returnObj = {}
    Object.keys(obj).forEach(function (key) {
      let content = obj[key]
      if (content && content.length > 0) {
        returnObj[key] = content
      }
    })
    return returnObj
  },
  ascendingDescendingSort (a, b, sortAttribute, order) {
    if (a[sortAttribute] < b[sortAttribute]) return order === ASCENDING ? -1 : 1
    if (a[sortAttribute] > b[sortAttribute]) return order === ASCENDING ? 1 : -1
    return 0
  },
  randomString () {
    return Math.random().toString(36).substring(2)
  },

  present (value) {
    if (typeof option === 'object') {
      if (value.keys().length > 0) {
        return true
      }
      return false
    }
    if (Array.isArray(value)) {
      return value && value.length > 0
    } else {
      return value && true
    }
  },
  ordinal (number) {
    const special = ['zeroth', 'first', 'second', 'third', 'fourth', 'fifth', 'sixth', 'seventh', 'eighth', 'ninth', 'tenth', 'eleventh', 'twelfth', 'thirteenth', 'fourteenth', 'fifteenth', 'sixteenth', 'seventeenth', 'eighteenth', 'nineteenth'];
    const deca = ['twent', 'thirt', 'fort', 'fift', 'sixt', 'sevent', 'eight', 'ninet']
    if (number < 20) return special[number]
    if (number % 10 === 0) return deca[Math.floor(number / 10) - 2] + 'ieth'
    return deca[Math.floor(number / 10) - 2] + 'y-' + special[number % 10]
  },
  imgToSvg ($imgs) {
    $imgs.each(function () {
      let $img = window.$(this)
      let $imgID = $img.attr('id')
      let $imgClass = $img.attr('class')
      let $imgURL = $img.attr('src')
      window.$.get($imgURL, function (data) {
          // Get the SVG tag, ignore the rest
        let $svg = jQuery(data).find('svg')

        // Add replaced image's ID to the new SVG
        if (typeof imgID !== 'undefined') {
            $svg = $svg.attr('id', imgID)
        }
        // Add replaced image's classes to the new SVG
        if (typeof imgClass !== 'undefined') {
            $svg = $svg.attr('class', imgClass + ' replaced-svg')
        }

        // Remove any invalid XML tags as per http://validator.w3.org
        $svg = $svg.removeAttr('xmlns:a');

        // Check if the viewport is set, else we gonna set it if we can.
        if (!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
          $svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'))
        }

        // Replace image with new SVG
        $img.replaceWith($svg)
      }, 'xml')
    })
  },
  buildSortParams (params, sortParams) {
    let sortOrder = null
    if (!sortParams || !sortParams.param || sortParams.param.length < 1) {
      return params
    }
    let order = sortParams.order || 'asc'
    order = order.toLowerCase()
    let param = sortParams.param
    params.sort = `${param} ${order}`
    return params
    if(sortParams.param == SURNAME){
      sortOrder = 'lastname'
    }
    if(sortParams.param == FIRSTNAME){
      sortOrder = 'firstname'
    }
    if(sortOrder){
      sortParams.order = sortParams.order || ASCENDING
      if(sortParams.order == DESCENDING){
        sortOrder = `-${sortOrder}`
      }
    }
    if(sortOrder){
      params.Sorts = sortOrder      
    }
    return params
  },
  debounce (func, delay) {
    let inDebounce
    let debounce = this.debounce
    return function () {
      const context = this
      const args = arguments
      clearTimeout(debounce)
      inDebounce = setTimeout(() => func.apply(context, args), delay)
    }
  },
  // these methods help to interact with api
  buildFiltersQueryParams (params, filters) {
    filters.forEach(el => {
      let filter = params[el.name]
      if (!Array.isArray(filter)) {
        filter = params[el.name] = []
      }
      if (Array.isArray(el.values)) {
        filter.push(...el.values)
      }
    })
    return params
  }
}
