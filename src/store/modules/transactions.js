import stockRepo from '../../repo/stockRepo.js'
import transactionRepo from '../../repo/transactionRepo.js'
import * as types from '../mutation-types'
import {ASCENDING, DESCENDING, FIRSTNAME, SURNAME} from '../../utils/Constants.js'
import Utils from '../../utils/Utils.js'
import axios from 'axios'

// initial state
const state = {
  transactions: {
    data: [],
    meta: {page: 1},
    errors: [],
    state: 'DATA',
    filters: [],
    sortParams: {param: null, order: ASCENDING}
  },
  stock: {
    data: [],
    meta: {page: 1},
    errors: [],
    state: 'DATA',
    filters: [],
    sortParams: {param: null, order: ASCENDING}
  }
}

// getters
const getters = {
  stock: state => state.stock.data,
  stockFilters: state => state.stock.filters,
  stockState: state => state.stock.state,
  // for the viewstate to always show the right view always set the state before the data
  // the viewstate is slightly different from the state
  // the view state is specifically used by the view to render the appropriate screen
  // for eg. a state of loading will have a viewstate of data if there are Stock while 
  // they are loading
  stockViewState: state => {
    let stockSubModule = state.stock
    let hasStock = stockSubModule.data && stockSubModule.data.length > 0
    if (hasStock) {
      return 'DATA'
    }
    if (stockSubModule.state !== 'LOADING' && stockSubModule.state !== 'ERROR') {
      return 'NO-DATA'
    }
    return stockSubModule.state
  },
  stockMeta: state => state.stock.meta,
  transactions: state => state.transactions.data
}

// actions
const actions = {
  getStock ({ commit, state, rootState }) {
    console.info('getting')
    let params = {Page: 1}
    // params = Utils.buildFiltersQueryParams(params, state.Stock.filters)
    // params = Utils.buildSortParams(params, state.Stock.sortParams)
    commit(types.SET_STOCK_STATE, 'LOADING')
    return stockRepo.get(params)
    .then((data) => {
      commit(types.RECEIVE_STOCK, data)
    }, errors => {
      commit(types.RECEIVE_STOCK, {errors})
    })
  },
  loadMoreStock ({commit, state}, page) {
    let params = {Page: page}
    params = Utils.buildFiltersQueryParams(params, state.stock.filters)
    params = Utils.buildSortParams(params, state.stock.sortParams)
    console.info(params)
    commit(types.SET_STOCK_STATE, 'LOADING')
    return stockRepo.get(params)
    .then(data => {
      commit(types.RECEIVE_MORE_STOCK, data)
    })
  },
  getTransactions ({commit,state,rootState}) {
    console.info('getting')
    let params = {Page: 1}
    // params = Utils.buildFiltersQueryParams(params, state.Stock.filters)
    // params = Utils.buildSortParams(params, state.Stock.sortParams)
    commit(types.SET_STOCK_STATE, 'LOADING')
    return transactionRepo.get(params)
    .then((data) => {
      commit(types.RECEIVE_TRANSACTIONS, data)
    }, errors => {
      commit(types.RECEIVE_TRANSACTIONS, {errors})
    })
  }
}

// mutations
const mutations = {

  [types.INIT_STOCK] (state) {
    // state.Stock.filters = []
    state.stock.sortParams = {param: null, order: ASCENDING}
  },
  /**
   * @param {Object} data - contains Stock, meta and errors keys
    */
  //  Todo rename to SET_STOCK
  [types.RECEIVE_STOCK] (state, {stock, meta, errors}) {
    if (errors) {
      state.stock.state = 'ERROR'
      state.stock.errors = errors
    } else {
      state.stock.state = 'DATA'
      state.stock.data = stock
      state.stock.meta = meta
    }
  },
  [types.RECEIVE_TRANSACTIONS] (state, {transactions, meta, errors}) {
    if (errors) {
      state.transactionS.state = 'ERROR'
      state.transactions.errors = errors
    } else {
      state.transactions.state = 'DATA'
      state.transactions.data = transactions
      state.transactions.meta = meta
    }
  },
  [types.SET_STOCK_STATE] (state, value) {
    state.stock.state = value
  },
  // Todo rename to append Stock used for pagination
  [types.RECEIVE_MORE_STOCK] (state, {stock, meta, errors}) {
    state.stock.data = state.stock.data.concat(stock)
    state.stock.meta = meta
    state.stock.state = 'DATA'
  },
  [types.ADD_STOCK_FILTER] (state, {name}) {
    let key = Utils.randomString()
    let filter = {key: key, name: name, values: []}
    state.stock.filters.push(filter)
  },
  [types.ADD_STOCK_FILTER_VALUE] (state, {key, value}) {
    let filter = state.stock.filters.find(el => key === el.key)
    filter.values.push(value)
  },
  [types.REMOVE_STOCK_FILTER] (state, {key}) {
    let filter = state.stock.filters.find(el => key === el.key)
    let index = state.stock.filters.indexOf(filter)
    state.stock.filters.splice(index, 1)
  },
  [types.REMOVE_STOCK_FILTER_VALUE] (state, {key, value}) {
    let filter = state.stock.filters.find(el => key === el.key)
    let filterValues = filter.values
    const index = filterValues.indexOf(value)
    filterValues.splice(index, 1)
  },
  [types.CLEAR_STOCK_FILTER] (state, {key}) {
    let filter = state.stock.filters.find(el => key === el.key)
    filter.values.splice(0)
  },
  [types.SET_STOCK_SORT_PARAMS] (state, sortParams) {
    state.stock.sortParams = sortParams
  },
  SET_TRANSACTIONS (state, transactions) {
    state.transactions = transactions
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
