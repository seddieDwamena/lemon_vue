'use strict'
require('./check-versions')()

process.env.NODE_ENV = 'production'

const ora = require('ora')
const rm = require('rimraf')
const path = require('path')
const chalk = require('chalk')
const webpack = require('webpack')
const config = require('../config')
const webpackConfig = require('./webpack.prod.conf')

const spinner = ora('building for production...')
spinner.start()

rm(path.join(config.build.assetsRoot, config.build.assetsSubDirectory), err => {
  if (err) throw err
  webpack(webpackConfig, function (err, stats) {
    spinner.stop()
    if (err) throw err
    process.stdout.write(stats.toString({
      colors: true,
      modules: false,
      children: false,
      chunks: false,
      chunkModules: false
    }) + '\n\n')

    if (stats.hasErrors()) {
      console.log(chalk.red('  Build failed with errors.\n'))
      process.exit(1)
    }

    // copy index.html to 200.html for surge
    const fs = require('fs')
    let indexFile = config.build.index
    let catchAllFile = indexFile.replace(/index(?!.*index)/, "200")
    fs.createReadStream(indexFile).pipe(fs.createWriteStream(catchAllFile));

    console.log(chalk.cyan('  Build complete.\n'))
    console.log(chalk.yellow(
      '  Tip: built files are meant to be served over an HTTP server.\n' +
      '  Opening index.html over file:// won\'t work.\n'
    ))

    // deploy files to surge
    // const surgeSpinner = ora('uploading to surge...')
    // surgeSpinner.start()
    // const exec = require('child_process').exec
    // var surge = exec('echo "caleb" | ./hello.sh'
    // var url = config.build.surgeUrl
    // var folder = indexFile.replace(/index\.html(?!.*index\.html)/, "")
    // var surge = exec(`surge ${folder} ${url}`, (e, so, se) => {
    //   surgeSpinner.stop()
    //   if(e){
    //     console.error(se)
    //   }else{
    //     console.log(so)
    //   }
    // })
  })
})
