//server
// export const BASE_URL = 'localhost:4000'
export const BASE_URL = 'http://lemon-api.nfortics.com'
// export const BASE_URL = 'http://556bc805.ngrok.io'
// export const API_URL = `${BASE_URL}/api/`
export const API_URL = `${BASE_URL}/api/v1/`
// export const FAKE_API_URL = `http://52.29.65.252:5005/`
export const FAKE_API_URL = `http://localhost:8088/`

export const FLOPAY_API_URL = `https://api.flopay.io/v1/`

export const BEIGE_BACKEND_GEORGE = `https://lemon-dashboard-api.nfortics.com/api/v1/`
export const BEIGE_BACKEND = `https://lemon-dashboard-api.nfortics.com/api/v1/`
export const BEIGE_BACKEND_ANDY = `https://lemon-dashboard-api.nfortics.com/api/v1/`
export const LOCAL_URL = `http://localhost:3000/api/v1/`
export const LEMON_DASHBOARD_GEORGE_URL = `http://b1f71524.ngrok.io/api/v1/`

export const NGROK_URL = `http://33ae056d.ngrok.io/api/v1/`
export const CURRENT_URL = BEIGE_BACKEND
export const CURRENT_URLv2 = CURRENT_URL.replace('v1', 'v2')

export const ngrok_url = CURRENT_URL

// sort orders
export const ASCENDING = 1
export const DESCENDING = 2

// search params
export const TRANSACTIONS = 1
export const SELF_SERVICE = 2
export const CONTACTS = 3
export const AGENTS = 4

// contact sort parameters
export const SURNAME = 'Surname'
export const FIRSTNAME = 'First name'

// unreconciled transaction errors
export const BLUE_ERROR = "0"
export const RED_ERROR = "1"
export const GRAY_ERROR = "2"
