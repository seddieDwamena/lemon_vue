import Noty from 'noty'
Noty.setMaxVisible(2)

export default {
  success: function (text, params) {
    params = Object.assign(params || {}, {text: text, type: 'success', timeout: 1500})
    return new Noty(params)
    .show()
  },
  info: function (text, params) {
    params = Object.assign(params || {}, {text: text, type: 'info', timeout: 1500})
    return new Noty(params)
    .show()
  },
  error: function (text, params) {
    params = Object.assign(params || {}, {text: text, type: 'error', timeout: 1500})
    return new Noty(params)
    .show()
  },
  noty: function (params) {
    return new Noty(params).show()
  }
}
