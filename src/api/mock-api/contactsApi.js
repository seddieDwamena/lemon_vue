import axios from 'axios';
import {BASE_URL} from '../../utils/Constants.js'
import Utils from '../../utils/Utils.js'
import Contact from '../../models/Contact.js'

var faker = require('faker');
let _contacts = []
for (var i = 0; i < 50; i++){
  let contact = { 
      id: i+1,
      branch_name: faker.address.city(),
      group_account: faker.company.companyName(),
      personal_info: {
        first_name: faker.name.firstName(),
        last_name: faker.name.lastName(),
        is_active: faker.random.boolean(),
        date_birth: faker.date.recent(),
        gender: "Male",
        bill_code: faker.random.alphaNumeric(),
        primary: faker.phone.phoneNumberFormat(),
        locality: faker.address.state(),
        size: faker.random.number()
      },
      relationshipOfficer: faker.name.findName(),
      accounts: [
        {
          name: "Cedi Account",
          number: faker.random.number()
        },
        {
          name: "Cedi Account",
          number: faker.random.number()
        }
      ]
  }; 
  //console.log("cc", contact)
  _contacts.push(contact)
}
export default {
  getContacts (params) {
    // const URL = `${BASE_URL}customers`
    // params.account_code = "mint"
    // if(!params.page){
    //   params.page = 1
    // }
    // params.per_page = 10
    // return axios.get(URL, {params: params})
    // .then(response => {
    //   let data =  response.data.data.map(el => {
    //     return new Contact(el)
    //   })
    //   return {data: data, meta: response.data.meta.table}
    // })
    return Promise.resolve({
      customers: _contacts.map(el => new Contact(el)),
       meta: {current_page: 1, total_pages: 1}
    })
  },
  findContact (id) {
    // const URL = `${BASE_URL}customers/${id}`
    // return axios.get(URL)
    // .then(response => {
    //   let data =  response.data.data
    //   return new Contact(data)
    // })

    return new Promise((resolve, reject) => {
      let contact = _contacts.find( (el) => {return el.id == id})
      resolve(new Contact(contact))
    })
  },
  // findContact (id) {
  //   const URL = `${BASE_URL}Contact/${id}`
  //   return axios.get(URL)
  //   .then(response => {
  //     let data =  response.data
  //     console.error("ssh", data)
  //     return this.normalizeContact(data)
  //   })

  // },
  normalizeContact (contact) {
    contact.name = Utils.concatString([contact.title, contact.firstName, contact.lastName], ' ')
    contact.status = contact.isActive ? 'Active': 'Inactive'
    return contact
  },
  normalizeContacts (contacts) {

  }

}
