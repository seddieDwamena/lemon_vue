import contactsRepo from '../../repo/contactsRepo.js'
import * as types from '../mutation-types'
import * as cacheTypes from '@/services/cache-types'
import { ASCENDING, DESCENDING } from '../../utils/Constants.js'
import Utils from '../../utils/Utils.js'
import cache from '@/services/cache'
import { CURRENT_URL } from '@/utils/Constants'
import axios from 'axios'
// import { stat } from 'fs';

// initial state
const state = {
  contacts: {
    data: [],
    meta: { page: 1 },
    errors: [],
    state: 'DATA',
    filters: [],
    sortParams: { param: null, order: ASCENDING }
  },
  currentContact: {
    data: null,
    state: null
  },
  filteredContacts: [],
  currentQuery: {}
}

// getters
const getters = {
  contacts: state => state.contacts.data,
  contactsMeta: state => state.contacts.meta,
  currentContact: state => state.currentContact.data,
  contactsFilters: state => state.contacts.filters,
  contactsViewState: state => {
    let contactsSubModule = state.contacts
    let hasContacts =
      contactsSubModule.data && contactsSubModule.data.length > 0
    if (hasContacts) {
      return 'DATA'
    }
    if (
      contactsSubModule.state !== 'LOADING' &&
      contactsSubModule.state !== 'ERROR'
    ) {
      return 'NO-DATA'
    }
    return contactsSubModule.state
  },
  currentContactViewState: state => {
    return state.currentContact.state
  }
}

// actions
const actions = {
  getContacts ({ commit, state }) {
    let params = {Page: 1}
    params = Utils.buildFiltersQueryParams(params, state.contacts.filters)
    params = Utils.buildSortParams(params, state.contacts.sortParams)
    commit(types.SET_CONTACTS_STATE, 'LOADING')
    return contactsRepo.get(params).then(
      data => {
        console.info('darl', data)
        commit(types.RECEIVE_CONTACTS, data)
      },
      errors => {
        console.info('darl', errors)
        commit(types.RECEIVE_CONTACTS, { errors })
      }
    )
  },
  loadMoreContacts ({ commit, state }, page) {
    let params = { Page: page }
    params = Utils.buildFiltersQueryParams(params, state.contacts.filters)
    params = Utils.buildSortParams(params, state.contacts.sortParams)
    commit(types.SET_CONTACTS_STATE, 'LOADING')
    return contactsRepo.get(params).then(contacts => {
      commit(types.RECEIVE_MORE_CONTACTS, contacts)
    })
  },
  // findContact ( { commit }, id) {
  //   commit(types.CLEAR_CURRENT_CONTACT)
  //   contactsRepo.findContact(id)
  //   .then( contact => {
  //     commit(types.RECEIVE_CONTACT, contact)
  //   })
  // },
  findContact ({ commit }, id) {
    let contact = cache.get(cacheTypes.CURRENT_CONTACT, id)
    if (contact) {
      commit(types.RECEIVE_CONTACT, contact)
      return Promise.resolve(true)
    }
    commit('SET_CONTACT_STATE', 'LOADING')
    return contactsRepo.findContact(id).then(contact => {
      commit(types.RECEIVE_CONTACT, contact)
      return true
    })
  },
  approveFarmer ({ commit }, id) {
    let url = `${CURRENT_URL}farmers/approve`
    axios.post(url, { id: id }).then(() => {
      commit('SET_CURRENT_CONTACT_STATUS', 'active')
    })
  }
}

// mutations
const mutations = {
  [types.RECEIVE_CONTACTS] (state, { contacts, meta, errors }) {
    if (errors) {
      state.contacts.state = 'ERROR'
      state.contacts.errors = errors
    } else {
      state.contacts.state = 'DATA'
      state.contacts.data = contacts
      state.contacts.meta = meta
    }
  },
  [types.RECEIVE_MORE_CONTACTS] (state, { contacts, meta, errors }) {
    state.contacts.state = 'DATA'
    state.contacts.data = state.contacts.data.concat(contacts)
    state.contacts.meta = meta
  },
  [types.SET_CONTACTS_STATE] (state, value) {
    state.contacts.state = value
  },
  [types.RECEIVE_CONTACT] (state, contact) {
    state.currentContact.data = contact
    state.currentContact.state = 'DATA'
    cache.set(cacheTypes.CURRENT_CONTACT, contact, contact.id)
  },
  SET_CONTACT_STATE (state, value) {
    state.currentContact.state = value
  },
  [types.SET_CONTACT] (state, contact) {
    state.currentContact.data = contact
    state.currentContact.state = 'DATA'
    cache.set(cacheTypes.CURRENT_CONTACT, contact, contact.id)
  },
  [types.ADD_CONTACTS_FILTER] (state, { name, displayName, options, type }) {
    let key = Utils.randomString()
    let filter = {
      key: key,
      name: name,
      values: [],
      displayName: displayName,
      options: options,
      type: type
    }
    state.contacts.filters.push(filter)
  },
  [types.ADD_CONTACTS_FILTER_VALUE] (state, { key, value }) {
    let filter = state.contacts.filters.find(el => key === el.key)
    filter.values.push(value)
  },
  [types.REMOVE_CONTACTS_FILTER] (state, { key }) {
    let filter = state.contacts.filters.find(el => key === el.key)
    let index = state.contacts.filters.indexOf(filter)
    state.contacts.filters.splice(index, 1)
  },
  [types.REMOVE_CONTACTS_FILTER_VALUE] (state, { key, value }) {
    let filter = state.contacts.filters.find(el => key === el.key)
    let filterValues = filter.values
    const index = filterValues.indexOf(value)
    filterValues.splice(index, 1)
  },
  [types.CLEAR_CONTACTS_FILTER] (state, { key }) {
    let filter = state.contacts.filters.find(el => key === el.key)
    filter.values.splice(0)
  },
  [types.CLEAR_CURRENT_CONTACT] (state) {
    state.currentContact = { data: null }
  },
  [types.ADD_TO_CART] (state, { id }) {
    state.all.find(product => product.id === id).inventory--
  },
  SET_CURRENT_CONTACT_STATUS (state, status) {
    if (state.currentContact.data) {
      state.currentContact.data.status = status
      state.currentContact.data.updated_at = Date.now()
      if (Array.isArray(state.contacts.data)) {
        let contact = state.contacts.data.find(
          el => el.id === state.currentContact.id
        )
        if (contact) {
          contact.status = status
        }
      }
    }
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
