/* eslint-disable react/react-in-jsx-scope */
import Vue from 'vue';
import { storiesOf } from '@storybook/vue';
import StoryRouter from 'storybook-router';
import { action } from '@storybook/addon-actions';
import { linkTo } from '@storybook/addon-links';

import PropsProvider from './PropsProvider.js'

import MyButton from './MyButton.vue';
import Welcome from './Welcome.vue';
import StoryBookApp from './StoryBookApp.vue';
import BreadCrumbComponent from '../components/BreadCrumbComponent.vue';
import NameFilterDropDown from '../components/NameFilterDropDown.vue';
import SearchWithDropdown from '../components/SearchWithDropdown.vue';
//import TopNav from '../components/TopNav.vue';
import Avatar from '../components/Avatar.vue';
import SideNav from '../components/SideNav.vue';
import MainContent from '../components/MainContent.vue';
import Clients from '../pages/Clients.vue';
import MapTransactions from './MapTransactions.vue';
import MapDiagram from './MapDiagram.vue';
import fakerTest from './fakerTest.vue';
// import Contacts from '../pages/client/Contacts.vue';
// import ContactTable from '../components/ContactTable.vue';
// import ContactDetails from '../components/ContactDetails.vue';
import TableRow from '../components/TableRow.vue'
import CheckBox from '../components/CheckBox.vue'
import TwoPaneContainer from '../components/TwoPaneContainer.vue'
import CustomerInfo from '../components/CustomerInfo.vue'
import InfoBox from './InfoBox.vue'
import ToggleButton from './ToggleButton.vue'



Vue.component('bread-crumb-component', BreadCrumbComponent)

Vue.component('name-filter-dropdown', NameFilterDropDown)
Vue.component('search-with-dropdown', SearchWithDropdown)
Vue.component('avatar', Avatar)
Vue.component('side-nav', SideNav)
Vue.component('clients', Clients)
// Vue.component('contacts', Contacts)
// Vue.component('contacts-table', ContactTable)
Vue.component('table-row', TableRow)
Vue.component('checkbox', CheckBox)
Vue.component('two-panel-container', TwoPaneContainer)
Vue.component('customer-info', CustomerInfo)
//Vue.component('top-nav', TopNav)
Vue.component('map-transactions', MapTransactions);
Vue.component('faker-test', fakerTest);
Vue.component('map-diagram', MapDiagram);
Vue.component('info-box', InfoBox);
Vue.component('toggle-button', ToggleButton);

storiesOf('CheckBox', module).add('default', () => ({
  components: {StoryBookApp},
  template: `<story-book-app>
            <checkbox id="1"></checkbox>
            </story-book-app>`
}));

storiesOf('CustomerInfo', module).add('default', () => ({
  components: {StoryBookApp},
  template: `<story-book-app>
            <customer-info></customer-info>
            </story-book-app>`
}));

storiesOf('TwoPaneContainer', module).add('default', () => ({
  template: `<story-book-app>
              <two-panel-container  style="width: 900px; height: 400px;">
              <div  style="background: red" slot="pane-one">Helloworld</div>
              <div style="background: blue" slot="pane-two">Helloworld</div>
            </two-panel-container>
            </story-book-app>`
}));

storiesOf('TableRow', module).add('default', () => ({
  components: {StoryBookApp},
  template: `<story-book-app >
            <table-row
            model="{id: 1, name: 'Sandra Rita Agbesi',
            primary: '+233 (0)24 567 9081', branch: 'Achimota', locality: 'Mile 7',
            status: 'Active', hello: 15, hi: 'This grand '}">
             </table-row>
            </story-book-app>`
}));
storiesOf('Clients', module)
.addDecorator(StoryRouter())
.add('default', () => ({
  components: {StoryBookApp, Clients},
  template: `<story-book-app>
            <clients></clients>
            </story-book-app>`
}));

storiesOf('Contacts', module)
  .addDecorator(StoryRouter())
  .add('default', () => ({
    components: {StoryBookApp, ContactTable, ContactDetails},
    template: `<story-book-app>
              <contacts></contacts>
              </story-book-app>`
}));

storiesOf('Contacts table', module)
  .addDecorator(StoryRouter())
  .add('default', () => ({
    components: {StoryBookApp},
    template: `<story-book-app>
              <contacts-table></contacts-table>
              </story-book-app>`
}));


storiesOf('SideNav', module).add('default', () => ({
  components: {StoryBookApp},
  template: `<story-book-app>
                <side-nav></side-nav>
            </story-book-app>`
}));

storiesOf('BreadCrumbComponent', module)
  .addDecorator(StoryRouter())
  .add('contacts', () => ({
    components: {BreadCrumbComponent, StoryBookApp},
    template: `<story-book-app>
              <bread-crumb-component without-form="none" titleLink="/contacts"
                  title="Contacts" subtitle="Sort contacts by"
                  export="Contacts" class="bread-div" id=""
                  >
              </bread-crumb-component>
              </story-book-app>`
}));

storiesOf('NameFilterDropDown', module).add('filter dropdown', () => ({
  components: {'name-filter-dropdown': NameFilterDropDown, StoryBookApp},
  template: `<story-book-app>
                <name-filter-dropdown title='Surname' :filters='["Surname", "First name"]' >
                </name-filter-dropdown>
            </story-book-app>`
}));

// storiesOf('Top Nav', module)
//   .addDecorator(StoryRouter())
//   .add('default', () => ({
//     components: {StoryBookApp, TopNav},
//     template: `<story-book-app>
//                   <top-nav >
//                   </top-nav>
//               </story-book-app>`
// }));

storiesOf('SearchWithDropdown', module).add('default', () => ({
  components: {StoryBookApp},
  template: `<story-book-app>
                <search-with-dropdown  >
                </search-with-dropdown>
            </story-book-app>`
}));

storiesOf('Button', module)
  .add('with text', () => ({
    components: { MyButton },
    template: '<my-button @click="action">Hello Button</my-button>',
    methods: { action: action('clicked') }
  }))
  .add('with JSX', () => ({
    components: { MyButton },
    render() {
      return <my-button onClick={this.action}>With JSX</my-button>;
    },
    methods: { action: linkTo('clicked') },
  }))
  .add('with some emoji', () => ({
    components: { MyButton },
    template: '<my-button @click="action">😀 😎 👍 💯</my-button>',
    methods: { action: action('clicked') },
  }));

/* eslint-enable react/react-in-jsx-scope */

storiesOf('MapTransactions', module).add('default', () => ({
  components: {StoryBookApp},
  template: `<story-book-app>
                <map-transactions>
                </map-transactions>
            </story-book-app>`
}));

storiesOf('MapDiagram', module).add('default', () => ({
  components: {StoryBookApp},
  template: `<story-book-app>
                <map-diagram>
                </map-diagram>
            </story-book-app>`
}));

storiesOf('fakerTest', module).add('default', () => ({
  components: {StoryBookApp},
  template: `<story-book-app>
                <faker-test>
                </faker-test>
            </story-book-app>`
}));

storiesOf('InfoBox', module).add('default', () => ({
  components: {StoryBookApp},
  template: `<story-book-app>
                <info-box>
                </info-box>
            </story-book-app>`
}));

storiesOf('ToggleButton', module).add('default', () => ({
  components: {StoryBookApp},
  template: `<story-book-app>
                <toggle-button>
                </toggle-button>
            </story-book-app>`
}));