import axios from 'axios';
import {API_URL, FAKE_API_URL, BEIGE_BACKEND, FLOPAY_API_URL,ngrok_url, CURRENT_URL} from '../utils/Constants.js'
import qs from 'qs'
const USE_FAKE_API = true
import faker from 'faker'
import store from '@/store/index'

export default {
  get (params) {
    let client_code = store.getters.user.client_code
    const URL = `${CURRENT_URL}/users?client_codes=${client_code}`
    let access_token = store.getters.token
    params.page = params.Page || 1
    params.limit = params.PageSize || 30
    // params.state = params.state || "Active"

    return axios.get(URL
    , {headers: {
        "Authorization": `Bearer ${access_token}`
      }
    },
    {params: params
      }
    )
    .then(data => {
      //console.info("FETCHED USERS")
      let users = data.users.map(el => {
        el.name = `${el.first_name} ${el.surname}`

        if (el.status === null)el.status = 'Active'
        else el.status = `${el.status}`

        if (el.balance === null || el.balance === '')el.balance = `GHc0.00`
        else el.balance = `GHc${el.balance}`
        if (el.phone_number === null)el.msisdn = faker.phone.phoneNumberFormat()
        else el.msisdn = el.phone_number

        if (el.email === null)el.email = `${el.first_name}.${el.surname}@gmail.com`
        else el.email = `${el.email}`

        return el
      })
      // console.log("REturned users",users)
      return {users: users,
        meta: {Page: 1,
          PageSize: data.users.length,
          TotalPages: 1,
          TotalFilteredRecords: data.users.length,
          TotalRecords: data.users.length
        }
      }
    })
  },
  find (id) {
    const URL = USE_FAKE_API ? `${ngrok_url}agents/${id}` : `${API_URL}agents/${id}`
    let access_token = store.getters.token
    return axios.get(URL,{headers: {
      'Authorization': `Bearer ${access_token}`
    }
    })
    .then(data => {
      // if(response.status != 200){
      //   throw "Failed to fetch data"
      // }
      let agent = data
      // TODO: the uuid id distinction should probably be from the server to avoid confusion alert andy
      agent.uuid = agent.id
      agent.id = id
      agent.type = agent.is_own_agent && !agent.is_super_agent ? 'PC' : 'DM'
      agent.phone = agent.registered_phone_number ? agent.registered_phone_number.number : null
      return agent
    })
  }

}
