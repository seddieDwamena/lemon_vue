/**
 * Mocking client-server processing
 */
var faker = require('faker');
let _agents = []
let sup = ['Bayport','Onango','eTranzact']
for (var i = 0; i < 50; i++){
  var rand = Math.floor(Math.random() * 3) + 0; 
  let agent = {
      id: i+1,
      firstname: faker.name.firstName(), 
      lastname: faker.name.lastName(),
      phone: faker.phone.phoneNumberFormat(),
      branch: faker.address.city(),
      location: faker.address.state(),
      status: faker.random.boolean(),
      type: faker.random.boolean() ? "Field Agent" : "Third Party",
      supplier: sup[rand],
      isNew: faker.random.boolean(),
      state: faker.random.boolean()
  };
  _agents.push(agent)
}
export default {
  get (params) {
    let page = params.page || 1 
    page = page < 1 ? 1 : page
    let perPage = params["per_Page"] || 10
    let query = params["query"]
    let branches = params.filters.Branch
    let status = params.filters.Status
    let location = params.filters.Location
    let agents = _agents.map( el => {
      return {
        id: el.id,
        firstname: el.firstname,
        lastname: el.lastname,
        phone: el.phone,
        branch: el.branch,
        location: el.location,
        status: el.status,
        type: el.type,
        supplier: el.supplier,
        isNew: el.isNew,
        state: el.state
      }
    })
    if(query){
      query = query.toUpperCase()
      agents = agents.filter( el => {
        return el.firstname.toUpperCase().includes(query) ||  el.lastname.toUpperCase().includes(query)
      })
    }
    if(location){
      agents = agents.filter( el => {
        let include = false;
        for(i = 0 ; i < location.length; i++){
          if(el.location.toUpperCase().includes(location[i].toUpperCase())){
            include = true
            break
          }
        }
        return include
      })
    }
    if(status){
      agents = agents.filter( el => {
        let include = false;
        for(i = 0 ; i < status.length; i++){
          if(el.status.toUpperCase().includes(status[i].toUpperCase())){
            include = true
            break
          }
        }
        return include
      })
    }
    let totalRecords = agents.length
    let startingIndex = (page - 1) * perPage
    let endingIndex = startingIndex + perPage
    console.error(startingIndex, endingIndex)
    return new Promise(function(resolve, reject) {
      setTimeout(resolve, 100, {
        data: agents.take(startingIndex, endingIndex).map(el => {
          el.name = `${el.firstname} ${ el.lastname}`
          return el
        }),
        meta: {page: page, perPage: perPage, totalRecords: totalRecords}
      })
    })
  },
  find (id) {
    let agent =  _agents.find( (el) => {return el.id == id})
    agent.name = `${agent.firstname} ${ agent.lastname}`
    return new Promise(function(resolve, reject) {
      setTimeout(resolve, 200,agent);
    });
  }

}
