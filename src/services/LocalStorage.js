// properties
let LocalStorage = function (obj) {
  this.supported = typeof (Storage) !== 'undefined'
}

// methods
LocalStorage.prototype.get = function (key, defaultValue) {
  if (this.supported) {
    let value = localStorage.getItem(key)
    if (value === 'undefined') {
      if (defaultValue) {
        this.set(key, defaultValue)
        return defaultValue
      }
      return undefined
    } else if (value == null) {
      if (defaultValue) {
        this.set(key, defaultValue)
        return defaultValue
      }
      return null
    } else {
      return JSON.parse(value)
    }
  } else {
    return defaultValue
  }
}
LocalStorage.prototype.set = function (key, value) {
  localStorage.setItem(key, JSON.stringify(value))
}

export default LocalStorage

const db = new LocalStorage()
export {db}
