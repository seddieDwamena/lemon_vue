export const CHANGE_CLIENT_SEARCH_PARAMETER = 'CHANGE_CLIENT_SEARCH_PARAMETER'

export const RECEIVE_GROUPS = 'RECEIVE_GROUPS'
export const SET_REGIONS = 'SET_REGIONS'
// AGENTS
export const INIT_AGENTS = 'INIT_AGENTS'
export const SET_AGENTS_STATE = 'SET_AGENTS_STATE'
export const RECEIVE_AGENTS = 'RECEIVE_AGENTS'
export const RECEIVE_MORE_AGENTS = 'RECEIVE_MORE_AGENTS'

export const ADD_AGENTS_FILTER_VALUE = 'ADD_AGENTS_FILTER_VALUE'
export const REMOVE_AGENTS_FILTER_VALUE = 'REMOVE_AGENTS_FILTER_VALUE'
export const CLEAR_AGENTS_FILTER = 'CLEAR_AGENTS_FILTER'
export const REMOVE_AGENTS_FILTER = 'REMOVE_AGENTS_FILTER'
export const RECEIVE_AGENT = 'RECEIVE_AGENT'
export const SET_AGENT = 'SET_AGENT'
export const ADD_AGENTS_FILTER = 'ADD_AGENTS_FILTER'
export const SET_AGENT_SORT_PARAMS = 'SET_AGENT_SORT_PARAMS'

// AGENT
export const CLEAR_CURRENT_AGENT = 'CLEAR_CURRENT_AGENT'
export const SET_CURRENT_AGENT_TRANSACTIONS_STATE = 'SET_CURRENT_AGENT_TRANSACTIONS_STATE'
export const RECEIVE_AGENT_TRANSACTIONS = 'RECEIVE_AGENT_TRANSACTIONS'
export const CLEAR_CURRENT_AGENT_TRANSACTIONS = 'CLEAR_CURRENT_AGENT_TRANSACTIONS'

// USERS
export const INIT_USERS = 'INIT_USERS'
export const SET_USERS_STATE = 'SET_USERS_STATE'
export const RECEIVE_USERS = 'RECEIVE_USERS'
export const RECEIVE_MORE_USERS = 'RECEIVE_MORE_USERS'

export const ADD_USERS_FILTER_VALUE = 'ADD_USERS_FILTER_VALUE'
export const REMOVE_USERS_FILTER_VALUE = 'REMOVE_USERS_FILTER_VALUE'
export const CLEAR_USERS_FILTER = 'CLEAR_USERS_FILTER'
export const REMOVE_USERS_FILTER = 'REMOVE_USERS_FILTER'
export const RECEIVE_USER = 'RECEIVE_USER'
export const SET_USER = 'SET_USER'
export const ADD_USERS_FILTER = 'ADD_USERS_FILTER'
export const SET_USER_SORT_PARAMS = 'SET_USER_SORT_PARAMS'

// USER
export const CLEAR_CURRENT_USER = 'CLEAR_CURRENT_USER'

// CONTACTS
export const INIT_CONTACTS = 'INIT_CONTACTS'
export const SET_CONTACTS_STATE = 'SET_CONTACTS_STATE'
export const RECEIVE_CONTACTS = 'RECEIVE_CONTACTS'
export const RECEIVE_MORE_CONTACTS = 'RECEIVE_MORE_CONTACTS'
export const SET_CONTACT = 'SET_CONTACT'

export const ADD_CONTACTS_FILTER_VALUE = 'ADD_CONTACTS_FILTER_VALUE'
export const REMOVE_CONTACTS_FILTER_VALUE = 'REMOVE_CONTACTS_FILTER_VALUE'
export const CLEAR_CONTACTS_FILTER = 'CLEAR_CONTACTS_FILTER'
export const REMOVE_CONTACTS_FILTER = 'REMOVE_CONTACTS_FILTER'
export const RECEIVE_CONTACT = 'RECEIVE_CONTACT'
export const ADD_CONTACTS_FILTER = 'ADD_CONTACTS_FILTER'
export const SET_CONTACT_SORT_PARAMS = 'SET_CONTACT_SORT_PARAMS'

export const SET_CURRENT_CONTACT_STATUS = 'SET_CURRENT_CONTACT_STATUS'

// STOCK
export const INIT_STOCK = 'INIT_STOCK'
export const SET_STOCK_STATE = 'SET_STOCK_STATE'
export const RECEIVE_STOCK = 'RECEIVE_STOCK'
export const RECEIVE_MORE_STOCK = 'RECEIVE_MORE_STOCK'

export const ADD_STOCK_FILTER_VALUE = 'ADD_STOCK_FILTER_VALUE'
export const REMOVE_STOCK_FILTER_VALUE = 'REMOVE_STOCK_FILTER_VALUE'
export const CLEAR_STOCK_FILTER = 'CLEAR_STOCK_FILTER'
export const REMOVE_STOCK_FILTER = 'REMOVE_STOCK_FILTER'
export const ADD_STOCK_FILTER = 'ADD_STOCK_FILTER'
export const SET_STOCK_SORT_PARAMS = 'SET_STOCK_SORT_PARAMS'

// BRANCHES
export const SET_SOCIETIES = 'SET_SOCIETIES'
export const ADD_SOCIETY = 'ADD_SOCIETY'
export const SET_DISTRICTS  = 'SET_DISTRICTS'
export const SET_TOWNS = 'SET_TOWNS'
export const SET_SECTORS = 'SET_SECTORS'