import store from '@/store'
import wsConnection from '@/services/wsConnection'
const CHANNEL_NAME = "FarmersChannel"
// properties
let FarmersChannel = function() {
  this.wsConnection = wsConnection
  this.handlers = []
  this.isSubscribed = false
}


FarmersChannel.prototype.handleMessage = function (message) {
    this.handlers.forEach(handler => {
        handler(message)
    });
}


FarmersChannel.prototype.subscribe = function (handler) {
    this.addHandler(handler) 
    let client_code = store.getters.user.client_code
    let identifier =  {channel: CHANNEL_NAME, code: client_code}
    this.wsConnection.subscribe(CHANNEL_NAME, CHANNEL_NAME, identifier, this.handleMessage.bind(this))
  
}

FarmersChannel.prototype.addHandler = function (handler) {
  this.handlers = this.handlers || []
  let handlers = this.handlers
  if(handlers.indexOf(handler) == -1){
    handlers.push(handler)
  }
}

FarmersChannel.prototype.unsubscribe = function (handler) {
    this.handlers = this.handlers || []
    let handlers = this.handlers
    let index = handlers.indexOf(handler)
    if(index != -1){
        handlers.splice(index, 1)
    }
    if(handlers.length == 0){
        this.wsConnection.unsubscribe(CHANNEL_NAME)
    }
  
}


const farmersChannel = new FarmersChannel()
export default farmersChannel

