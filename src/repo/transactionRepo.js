import transactionsApi from '../api/transactionsApi.js'

export default {
  get (agentId, params) {
    return transactionsApi.get(agentId, params)
  }
}
