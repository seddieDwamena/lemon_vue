import sectorsRepo from '../../repo/clientsRepo'
import * as types from '../mutation-types'
import {ASCENDING, CURRENT_URL} from '../../utils/Constants.js'
import Utils from '../../utils/Utils.js'
import axios from 'axios'
import store from '@/store/index'
import {db} from '@/services/LocalStorage'
// initial state
const state = {
  towns: {
    data: db.get('TOWNS', []),
    originalData: null,
    meta: {page: 1},
    errors: [],
    state: 'DATA',
    filters: [],
    sortParams: {param: null, order: ASCENDING},
    townCount: 0
  },
  currenttown: null
}

// getters
const getters = {
  towns: state => state.towns.data,
  townCount: state => state.towns.data.length,
  // for the viewstate to always show the right view always set the state before the data
  // the viewstate is slightly different from the state
  // the view state is specifically used by the view to render the appropriate screen
  // for eg. a state of loading will have a viewstate of data if there are towns while  
  // they are loading
  townsViewState: state => {
    let townsSubModule = state.towns
    let hasAgents = townsSubModule.data && townsSubModule.data.length > 0
    if (hasAgents) {
      return 'DATA'
    }
    if (townsSubModule.state !== 'LOADING' && townsSubModule.state !== 'ERROR') {
      return 'NO-DATA'
    }
    return townsSubModule.state
  },
  townsMeta: state => state.towns.meta
}

// actions
const actions = {
  getTowns ({ commit, state, rootState }) {
    let client_code = store.getters.user.client_code
    const URL = `${CURRENT_URL}towns?client_codes[]=${client_code}&limit=10&page=1`
    axios.get(
      URL
    ).then(r => r)
    .then((towns) => {
      commit('SET_TOWNS', towns)
    })
  }
}

// mutations
const mutations = {
  SET_TOWNS (state, towns) {
    if (Array.isArray(towns.towns)) {
      state.towns.data = towns.towns
      db.set('TOWNS', towns.towns)
      state.townCount = towns.towns.length
    }
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
