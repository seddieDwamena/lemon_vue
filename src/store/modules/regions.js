import clientsRepo from '../../repo/clientsRepo.js'
import * as types from '../mutation-types'
import {ASCENDING, DESCENDING} from '../../utils/Constants.js'
import Utils from '../../utils/Utils.js'

// initial state
const state = {
  regions: {
    data: [],
    meta: {page: 1},
    errors: [],
    state: 'DATA',
    filters: [],
    sortParams: {param: null, order: ASCENDING}
  }
}

// getters 
const getters = {
  regions: state => state.regions.data,
  regionsMeta: state => state.regions.meta,
  currentRegion: state => state.currentregion,
  RegionsFilters: state => state.regions.filters,
  RegionsViewState: state => {
    let regionsSubModule = state.regions
    let hasregions = regionsSubModule.data && regionsSubModule.data.length > 0
    if (hasregions) {
      return 'DATA'
    }
    if (regionsSubModule.state !== 'LOADING' && regionsSubModule.state !== 'ERROR') {
      return 'NO-DATA'
    }
    return regionsSubModule.state
  }
}

// actions
const actions = {
  getRegions ({ commit }) {
    let params = {Page: 1}
    commit(types.SET_REGIONS_STATE, 'LOADING')
    return clientsRepo.getRegions(params)
    .then((data) => {
      // console.info('darl', data)
      commit(types.RECEIVE_REGIONS, data)
    }, errors => {
      console.info('darl', errors)
      commit(types.RECEIVE_REGIONS, {errors})
    })
  },
  findContact ({ commit }, id) {
    return new Promise((resolve, reject) => {
      commit(types.CLEAR_CURRENT_AGENT)
      contactsRepo.findContact(id)
      .then(contact => {
        commit(types.RECEIVE_CONTACT, contact)
        resolve()
      })
    })
  }
}

// mutations
const mutations = {
  [types.RECEIVE_REGIONS] (state, {regions, meta, errors}) {
    if (errors) {
      state.regions.state = 'ERROR'
      state.regions.errors = errors
    } else {
      state.regions.state = 'DATA'
      state.regions.data = regions
      state.regions.meta = meta
    }
  },
  // [types.RECEIVE_MORE_CONTACTS] (state, {contacts, meta, errors}) {
  //   state.contacts.state = "DATA"
  //   state.contacts.data = state.contacts.data.concat(contacts)
  //   state.contacts.meta = meta
  // },
  [types.SET_REGIONS_STATE] (state, value) {
    state.regions.state = value
  }
  // [types.RECEIVE_CONTACT] (state, contact) {
  //   state.currentContact = contact
  // },
  // [types.SET_CONTACT] (state, contact) {
  //   state.currentContact = contact
  // },
  // [types.ADD_CONTACTS_FILTER] (state, {name}) {
  //   let key = Utils.randomString()
  //   let filter = {key: key, name: name, values: []}
  //   state.contacts.filters.push(filter)
  // },
  // [types.ADD_CONTACTS_FILTER_VALUE] (state, {key, value}) {
  //   let filter = state.contacts.filters.find( el => key == el.key)
  //   filter.values.push(value)
  // },
  // [types.REMOVE_CONTACTS_FILTER] (state, {key}) {
  //   let filter = state.contacts.filters.find( el => key == el.key)
  //   let index = state.contacts.filters.indexOf(filter)
  //   state.contacts.filters.splice(index, 1)
  // },
  // [types.REMOVE_CONTACTS_FILTER_VALUE] (state, {key, value}) {
  //   let filter = state.contacts.filters.find( el => key == el.key)
  //   let filterValues = filter.values
  //   const index = filterValues.indexOf(value);
  //   filterValues.splice(index, 1);
  // },
  // [types.CLEAR_CONTACTS_FILTER] (state, {key}) {
  //   let filter = state.contacts.filters.find( el => key == el.key)
  //   filter.values.splice(0)
  // },
  // [types.CLEAR_CURRENT_CONTACT] (state) {
  //   state.currentContact = null
  // },
  // [types.ADD_TO_CART] (state, { id }) {
  //   state.all.find(product => product.id === id).inventory--
  // }
}

export default {
  state,
  getters,
  actions,
  mutations
}
