import Vue from 'vue'
import Vuex from 'vuex'
// import * as actions from './actions'
import contacts from './modules/contacts'
import agents from './modules/agents'
import users from './modules/users'
import sectors from './modules/sectors'
import transactions from './modules/transactions'
import groups from './modules/groups'
import districts from './modules/districts'
import towns from './modules/towns'
import soceities from './modules/soceities'
// import client from '../repo/clientsRepo'
import * as types from './mutation-types'
import LocalStorage from '@/services/LocalStorage'
import axios from 'axios'
// import { ngrok_url } from '../utils/Constants';
import {CURRENT_URL} from '../utils/Constants.js'

Vue.use(Vuex)

// const debug = process.env.NODE_ENV !== 'production'
const db = new LocalStorage()

function fetchRegions (commit) {
  let URL = `${CURRENT_URL}regions`
  return axios.get(URL)
  .then(data => {
    commit(types.SET_REGIONS, data.regions)
  })
}

export default new Vuex.Store({
  actions: {
    fetchUserData ({state, commit}) {
      if (Array.isArray(state.regions) && state.regions.length > 0) {
        fetchRegions(commit)
        return Promise.resolve(state.regions)
      } else {
        return fetchRegions(commit)
      }
    
    },
    loadGroupPrivileges ({ commit }) {
      axios
        .get('http://003a64d7.ngrok.io/api/v1/privileges')
        .then(r => r.data)
        .then(privileges => {
          console.log('DATA:',privileges)
          commit('SET_PRIVILEGES', privileges)
        })
    }
    // fetchSectors = function ({commit}) {
    //   return axios.get(`${CURRENT_URL}sectors`)
    //   .then((data) => {
    //     commit(types.SET_SECTORS, data.sectors)
    //     return true
    //   })
    // },
    
    // fetchSocieties = function ({commit}) {
    //   return axios.get(`${CURRENT_URL}societies`)
    //   .then((data) => {
    //     commit(types.SET_SOCIETIES, data.societies)
    //     return true
    //   })
    // },
    // fetchDistricts = function ({commit}) {
    //   return axios.get(`${CURRENT_URL}districts`)
    //   .then((data) => {
    //     commit(types.SET_DISTRICTS, data.districts)
    //     return true
    //   })
    // },
    // fetchTowns = function ({commit}) {
    //   return axios.get(`${CURRENT_URL}towns`)
    //   .then((data) => {
    //     commit(types.SET_TOWNS, data.towns)
    //     return true
    //   })
    // }
  },
  modules: {
    contacts,
    agents,
    transactions,
    users,
    groups,
    sectors,
    districts,
    towns,
    soceities
  },
  state: {
    // three states : null, 'ERROR' AND 'LOADED'
    initialDataState: null,
    client: db.get('CLIENT', {}),
    token: db.get('TOKEN', null),
    user: null,
    regions: db.get('REGIONS', []),
    branches: db.get('BRANCHES', null),
    buyers: [
      {code: 'rc', name: 'Royal Commodities'}
    ],
    providers: ['mtn', 'vodafone', 'airtelTigo'],
    idTypes: ['Voter\'s ID', 'Health Insurance', 'Birth Certificate', 'Driver\'s Licence'],
    currentSearchParameter: '',
    sortParameters: {
      parameter: null,
      order: null
    },
    filterOptions: null,
    contactsSortParameters: {
      order: null,
      filter: null
    },
    agentsSortParameters: {
      order: null,
      filter: null
    },
    privileges: [],
    mapData: [],
    usersData: []
  },
  getters: {
    currentClientSearchParameter: state => state.currentClientSearchParameter,
    client: state => state.client,
    token: state => state.token,
    user: state => state.user,
    providers: state => state.providers,
    regions: state => state.regions,
    buyers: state => state.buyers,
    initialDataState: state => state.initialDataState,
    mapData: state => state.mapData,
    usersData: state => state.usersData,
    idTypes: state => state.idTypes
  },
  mutations: {
    [types.CHANGE_CLIENT_SEARCH_PARAMETER] (state, parameter) {
      state.currentSearchParameter = parameter
    },
    [types.SET_REGIONS] (state, regions) {
      if (Array.isArray(regions)) {
        state.regions = regions
        db.set('REGIONS', regions)
      }
    },
    SET_PRIVILEGES (state, privileges) {
      if (Array.isArray(privileges)) {
        state.privileges = privileges
      }
    },
    SET_AGENT_MAP_DATA (state, mapData) {
      state.mapData = mapData
    },
    SET_USER_MAP_DATA (state, usersData) {
      state.usersData = usersData
    },
    updateSortParameters (state, {parameter, order}) {
      state.sortParameters.parameter = parameter
      state.sortParameters.order = order
    },
    setInitialDataState (state, value) {
      state.initialDataState = value
    },
    // updateFilterOptions (state, filters) {
    //   this.filterOptions = filters
    // }
    getClient (state, client) {
      state.client = client
    },
    getUser (state, user) {
      state.user = user
    },
    setEvaBalance (state, amount) {
      if (state.user) {
        state.user.available_eva_balance = amount
      }
    },
    setToken (state, token) {
      state.token = token
      db.set('TOKEN', token)
    },
    setClient (state, client) {
      state.client = client
      db.set('CLIENT', client)
    },
    setUser (state, user) {
      state.user = user
    },
    setUsers (state, users) {
      state.users = users
    }

  }
})
