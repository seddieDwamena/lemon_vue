import store from '@/store'

// properties
let WsConnection = function () {
  this.ws = null
  this.channels = {}
};

WsConnection.prototype.handleMessage = function (message) {
  message = JSON.parse(message)
  if (!(message.identifier && message.message)){
    return
  }
  let channelName = JSON.parse(message.identifier).channel
  let channels = Object.keys(this.channels).filter(channelKey => {
    let channel = this.channels[channelKey]
    return channel.name === channelName && channel.isSubscribed
  })
  .map(channelKey => this.channels[channelKey])
  .forEach(channel => {
    channel.messageHandler(message.message)
  })
}
WsConnection.prototype.open = function () {
  if (!this.ws || this.ws.readyState > 1) {
    this.ws = new WebSocket(`wss://lemon-dashboard-api.nfortics.com/websocket`)
    return new Promise((resolve, reject) => {
      this.ws.onopen = (event) => {
                // resubscribe channels in case socket was closed unexpectedly
        this.channels.forEach((key, channel) => {
          if (channel.isSubscribed) {
            channel.isSubscribed = false
            this.sendSubscribeMessage(channel.name, channel.identifier)
          }
        })
        this.ws.onmessage = (event) => this.handleMessage(event.data)
        resolve(this)
      }
    })
  } else {
    return Promise.resolve(this)
  }
}

WsConnection.prototype.close = function () {
  this.ws.close()
}

WsConnection.prototype.sendSubscribeMessage = function (channel) {
  if (channel.isSubscribed) {
    return
  }
  let data = {identifier: JSON.stringify(channel.identifier), command: 'subscribe'}
  this.open()
  .then(() => {
    this.ws.send(JSON.stringify(data))
    channel.isSubscribed = true
  })
}

WsConnection.prototype.sendUnSubscribeMessage = function (channel) {
  if (!this.ws || this.ws.readyState > 1) {
    return
  }
  if (!channel || !channel.isSubscribed) {
    return
  }
  let data = {identifier: JSON.stringify(channel.identifier), command: 'unsubscribe'}
  this.ws.send(JSON.stringify(data))
  delete this.channels[channel.key]
}

WsConnection.prototype.subscribe = function (key, channelName, identifier, handler) {
  this.channels = this.channels || {}
  let channels = this.channels
  if (channels[key] && channels[key].isSubscribed) {
    return
  }
  let channel = channels[key] = {}
  channel.name = channelName
  channel.identifier = identifier
  channel.key = key
  channel.isSubscribed = false
  channel.messageHandler = handler
  this.sendSubscribeMessage(channel)
  return this
}

WsConnection.prototype.unsubscribe = function (key) {
  let channel = this.channels[key]
  if (!channel || !channel.isSubscribed) {
    return
  }
  this.sendUnSubscribeMessage(channel)
}

const wsConnection = new WsConnection()

export default wsConnection
