import { BASE_IMG_URL } from "@/utils/Constants";
import Vue from "vue";
import Utils from "@/utils/Utils";
import moment from "moment";
import store from "@/store";

Vue.filter("money", function(value, currency, options) {
  let symbol = "";
  options = options || {};
  currency = currency || "GHS";
  if (currency == "GHS") {
    symbol = "\u20B5";
  }
  if (options.symbol == "text") {
    symbol = "GHS";
  }
  return `${symbol} ${value}`;
});

Vue.filter("name", function(name) {
  if (!name) {
    return "";
  }
  return name.toLowerCase().replace(/\b\w/g, function(firstWord) {
    return firstWord.toUpperCase();
  });
});

Vue.filter("userType", function(type) {
  type = type || "";
  type = type.toLowerCase();
  if (type == "agent") {
    return "Purchasing Clerk";
  } else if (type == "backofficer") {
    return "District Manager";
  }
});

Vue.filter("activityModeId", function(credit_acc, debit_acc) {
  // for now just return the one that exists
  if (!credit_acc || credit_acc.length < 2) {
    return debit_acc;
  }
  return credit_acc;
});

Vue.filter("phone-no", function(phone) {
  if (!phone) {
    return;
  }
  return phone.replace(/^\+233|^233/gi, 0);
});

Vue.filter("region-name", function(id, regions) {
  let region = regions.find(el => el.id == id);
  return region ? region.name : null;
});

Vue.filter("town-name", function(id, towns) {
  let town = towns.find(el => el.id == id);
  return town ? town.name : null;
});

Vue.filter("society-name", function(id, societies) {
  let society = societies.find(el => el.id == id);
  return society ? society.name : null;
});

Vue.filter("crops-grown", function(farm) {
  if (
    farm.data &&
    farm.data.crops_grown &&
    Array.isArray(farm.data.crops_grown)
  ) {
    return farm.data.crops_grown.map(el => el.name).join(", ");
  }
  return farm.crops_grown_other;
});

Vue.filter("amt-unit", function(amount, unit) {
  if (!amount) {
    return null;
  }
  return `${amount} ${unit ? unit : 'kg'}`
});

Vue.filter("buyer", function(code, buyers) {
  let buyer = buyers.find(el => el.code == code);
  return buyer ? buyer.name : null;
});

Vue.filter("farm-size", function(size, unit) {
  unit = unit || "acres";
  if (!size) {
    return null;
  }
  return `${size} ${unit}`;
});

Vue.filter("present", function(value) {
  // if typeof option === 'object'
  if (Array.isArray(value)) {
    return value && value.length > 0;
  } else {
    return value && true;
  }
});

Vue.filter("img", function(img) {
  return `data:image/*;base64,${img}`;
});

Vue.filter("approval-status", function(status) {
  return Utils.approvalStatusName(status);
});

Vue.filter("ordinal", function(number) {
  return Utils.ordinal(number);
});

Vue.filter("fullname", function({ first_name, surname, title }) {
  return `${first_name} ${surname}`;
});

Vue.filter("age", function(dob) {
  if (!dob) {
    return null;
  }
  return moment().diff(dob, "years");
});

Vue.filter("farmer-status-info", function(contact) {
  if (!contact) {
    return null;
  }
  if (contact.status == "rejected") {
    let now = moment();
    let date = moment(contact.updated_at);
    if (date.years() === now.years()) {
      date = date.format("Do MMM");
    } else {
      date = date.format("Do MMM, YYYY");
    }
    return `Rejected by ${contact.rejected_by} on ${date}<br>Reason : ${
      contact.reason
    }`;
  } else {
    return null;
  }
});
