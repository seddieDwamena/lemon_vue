import sectorsRepo from '../../repo/clientsRepo'
import * as types from '../mutation-types'
import {ASCENDING, CURRENT_URL} from '../../utils/Constants.js'
import Utils from '../../utils/Utils.js'
import axios from 'axios'
import store from '@/store/index'

// initial state
const state = {
  districts: {
    data: [],
    originalData: null,
    meta: {page: 1},
    errors: [],
    state: "DATA",
    filters: [],
    sortParams: {param: null, order: ASCENDING},
    districtCount: 0
  },
  currentdistrict: null
}

// getters
const getters = {
  districts: state => state.districts.data,
//   currentdistrict: state => state.currentAgent,
//   districtsFilters: state => state.districts.filters,
//   districtsState: state => state.districts.state,
  districtCount: state => state.districts.data.length,
  // for the viewstate to always show the right view always set the state before the data
  // the viewstate is slightly different from the state
  // the view state is specifically used by the view to render the appropriate screen
  // for eg. a state of loading will have a viewstate of data if there are districts while  
  // they are loading
  districtsViewState: state => {
    let districtsSubModule = state.districts
    let hasAgents = districtsSubModule.data && districtsSubModule.data.length > 0
    if (hasAgents) {
      return 'DATA'
    }
    if (districtsSubModule.state !== 'LOADING' && districtsSubModule.state !== 'ERROR') {
      return 'NO-DATA'
    }
    return districtsSubModule.state
  },
  districtsMeta: state => state.districts.meta
}

// actions
const actions = {
  getDistricts ({ commit, state, rootState,rootGetters }) {
    let client_code = rootGetters.user.client_code
    const URL = `${CURRENT_URL}districts?client_codes[]=${client_code}&limit=10&page=1`
    axios.get(
      URL
    ).then(r => r)
    .then((districts) => {
        // let sectors = store.state.sectors.sectors.data;
        // console.log("Sectors from districts",sectors);
        // let districts = data.districts.map( el => {
        //     console.log("sector id",el.sector_id)
        //     var sec = sectors.filter(sector => sector.id == el.sector_id)
        //     el.sector_name = sec[0].name
        //     return el
        // })
      commit('SET_DISTRICTS', districts)
    })
  }
}

// mutations
const mutations = {
  SET_DISTRICTS (state, districts) {
    state.districts.data = districts.districts
    state.districtCount = districts.length
  },
  GET_SOCEITIES () {

  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
