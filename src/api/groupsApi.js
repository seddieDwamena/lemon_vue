import axios from 'axios';
import {API_URL,ngrok_url, CURRENT_URL} from '../utils/Constants.js'
//const USE_FAKE_API = true
import store from '@/store/index'

export default  {
  get (params) {
    // console.log("I got here")
    // alert()
    const URL = `${CURRENT_URL}user_groups`
    params.page = params.Page || 1
    params.limit = params.PageSize || 100
    // params.state = params.state || "Active"

    return axios.get(URL,
      {params: params
      }
    )
    .then(data => {
      let groups = data.user_groups
      return {groups: groups
        // meta: {Page: 1, PageSize: data.user_groups.length,
        //   TotalPages: 1, TotalFilteredRecords: data.user_groups.length,
        //   TotalRecords: data.user_groups.length
        // }
      }
    })
  },
  getPriv (params, type) {
    const URL = `${CURRENT_URL}privileges?privilege_types[]=${type}`
    let access_token = store.getters.token
    params.page = params.Page || 1
    params.limit = params.PageSize || 30
    // params.state = params.state || "Active"

    return axios.get(URL
    , {headers: {
        "Authorization": `Bearer ${access_token}`
      }
    },
    {params: params
      }
    )
    .then(data => {
      console.info('FETCHED USER Groups')
      let type = data.user_groups
      console.log('REturned user groups',data)
      return {groups: groups,
        meta: {Page: 1, PageSize: data.user_groups.length,
          TotalPages: 1, TotalFilteredRecords: data.user_groups.length,
          TotalRecords: data.user_groups.length
        }
      }
    })
  },
  find (id) {
    const URL = USE_FAKE_API ? `${CURRENT_URL}agents/${id}` : `${CURRENT_URL}agents/${id}`
    let access_token = store.getters.token
    return axios.get(URL)
    .then(data => {
      // if(response.status != 200){
      //   throw "Failed to fetch data"
      // }
      let agent = data
      // TODO: the uuid id distinction should probably be from the server to avoid confusion alert andy
      agent.uuid = agent.id
      agent.id = id
      agent.type = agent.is_own_agent && !agent.is_super_agent ? "PC" : "DM"
      agent.phone = agent.registered_phone_number ? agent.registered_phone_number.number : null
      return agent
    })
  }

}
