import agentsRepo from '../../repo/agentsRepo.js'
import transactionsRepo from '../../repo/transactionRepo.js'
import * as types from '../mutation-types'
import Utils from '../../utils/Utils.js'
import cache from '@/services/cache'
import * as cacheTypes from '@/services/cache-types'
import axios from 'axios'
import {CURRENT_URL} from '@/utils/Constants'
// initial state
const state = {
  agents: {
    data: [],
    meta: {page: 1},
    errors: [],
    state: 'DATA',
    filters: [],
    sortParams: {param: 'updated_at', order: 'DESC'}
  },
  currentAgent: null,
  currentAgentTransactions: {
    data: [],
    meta: {page: 1},
    errors: [],
    state: 'DATA'
  }
}

// getters
const getters = {
  agents: state => state.agents.data,
  currentAgent: state => state.currentAgent,
  agentsFilters: state => state.agents.filters,
  agentsSortParams: state => state.agents.sortParams,
  agentsState: state => state.agents.state,
  // for the viewstate to always show the right view always set the state before the data
  // the viewstate is slightly different from the state
  // the view state is specifically used by the view to render the appropriate screen
  // for eg. a state of loading will have a viewstate of data if there are agents while 
  // they are loading
  agentsViewState: state => {
    let agentsSubModule = state.agents
    let hasAgents = agentsSubModule.data && agentsSubModule.data.length > 0
    if (hasAgents) {
      return 'DATA'
    }
    if (agentsSubModule.state !== 'LOADING' && agentsSubModule.state !== 'ERROR') {
      return 'NO-DATA'
    }
    return agentsSubModule.state
  },
  agentsMeta: state => state.agents.meta,
  currentAgentTrans: state => state.currentAgentTransactions.data,
  currentAgentTransMeta: state => state.currentAgentTransactions.meta,
  currentAgentTransState: state => state.currentAgentTransactions.state,
  currentAgentTransViewState: state => {
    // or now always return no data
    return 'NO-DATA'
    let agentTransModule = state.currentAgentTransactions
    let hasTrans = agentTransModule.data && agentTransModule.data.length > 0
    if(hasTrans){
      return 'DATA'
    }
    if(agentTransModule.state != 'LOADING' && agentTransModule.state != 'ERROR'){
      return 'NO-DATA'
    }
    return agentTransModule.state
  }

}

// actions
const actions = {
  getAgents ({ commit, state, rootState }) {
    let params = {Page: 1}
    params = Utils.buildFiltersQueryParams(params, state.agents.filters)
    params = Utils.buildSortParams(params, state.agents.sortParams)
    commit(types.SET_AGENTS_STATE, 'LOADING')
    return agentsRepo.get(params)
    .then((data) => {
      commit(types.RECEIVE_AGENTS, data)
    }, errors => {
      commit(types.RECEIVE_AGENTS, {errors})
    })
  },
  createEditAgent ({commit, state}, {params, mode, id}) {
    let action
    if (mode === 'EDIT') {
      action = axios.put
    } else {
      action = axios.post
    }
    let url = `${CURRENT_URL}/agents`
    if (mode === 'EDIT') {
      url = `${url}/${id}`
    }
    return action(url, params)
    .then(({agent}) => {
      if (mode === 'EDIT') {
        commit(types.RECEIVE_AGENT, agent)
      }
      return agent
    })
  },
  loadMoreAgents ({commit, state}, page) {
    let params = {Page: page}
    params = Utils.buildFiltersQueryParams(params, state.agents.filters)
    params = Utils.buildSortParams(params, state.agents.sortParams)
    console.info(params)
    commit(types.SET_AGENTS_STATE, 'LOADING')
    return agentsRepo.get(params)
    .then(data => {
      commit(types.RECEIVE_MORE_AGENTS, data)
    })
  },
  findAgent ({commit, dispatch}, id) {
    let agent = cache.get(cacheTypes.CURRENT_AGENT, id)
    if (agent) {
      commit(types.RECEIVE_AGENT, agent)
      return Promise.resolve(true)
    }
    return agentsRepo.find(id)
      .then(agent => {
        commit(types.RECEIVE_AGENT, agent)
        // dispatch('getAgentTransactions', {})
      })
  },
  getAgentTransactions ({commit, state}) {
    let params = {Page: 1}
    let agentId = state.currentAgent.id
    commit(types.SET_CURRENT_AGENT_TRANSACTIONS_STATE, 'LOADING')
    commit(types.CLEAR_CURRENT_AGENT_TRANSACTIONS)
    return transactionsRepo.get(agentId, params)
      .then(data => {
        commit(types.RECEIVE_AGENT_TRANSACTIONS, data)
      })
  }
}

// mutations
const mutations = {

  [types.INIT_AGENTS] (state) {
    // state.agents.filters = []
    state.agents.sortParams = {param: 'updated_at', order: 'DESC'}
  },
  /**
   * @param {Object} data - contains agents, meta and errors keys
    */
  //  Todo rename to SET_AGENTS
  [types.RECEIVE_AGENTS] (state, {agents, meta, errors}) {
    if(errors){
      state.agents.state = 'ERROR'
      state.agents.errors = errors
    }else{
      state.agents.state = 'DATA'
      state.agents.data = agents
      state.agents.meta = meta
    }
  },
  [types.SET_AGENTS_STATE] (state, value) {
    state.agents.state = value
  },
  // Todo rename to append agents used for pagination
  [types.RECEIVE_MORE_AGENTS] (state, {agents, meta, errors}) {
    state.agents.data = state.agents.data.concat(agents)
    state.agents.meta = meta
    state.agents.state = 'DATA'
  },
  [types.RECEIVE_AGENT] (state, agent) {
    state.currentAgent = agent
    cache.set(cacheTypes.CURRENT_CONTACT, agent, agent.id)
  },
  [types.RECEIVE_AGENT_TRANSACTIONS] (state, {trans, meta, errors}) {
    if (errors) {
      state.currentAgentTransactions.state = 'ERROR'
      state.currentAgentTransactions.errors = errors
    } else {
      state.currentAgentTransactions.state = 'DATA'
      state.currentAgentTransactions.data = trans
      state.currentAgentTransactions.meta = meta
    }
  },
  [types.SET_CURRENT_AGENT_TRANSACTIONS_STATE] (state, value) {
    state.currentAgentTransactions.state = value
  },
  [types.CLEAR_CURRENT_AGENT_TRANSACTIONS] (state) {
    state.currentAgentTransactions.data = []
    state.currentAgentTransactions.meta = {page: 1}
    state.currentAgentTransactions.errors = []
  },
  [types.SET_AGENT] (state, agent) {
    state.currentAgent = agent
  },
  [types.ADD_AGENTS_FILTER] (state, {name, displayName, options, type}) {
    let key = Utils.randomString()
    let filter = {key: key, name: name, values: [], displayName: displayName, options: options, type: type}
    state.agents.filters.push(filter)
  },
  [types.ADD_AGENTS_FILTER_VALUE] (state, {key, value}) {
    let filter = state.agents.filters.find(el => key === el.key)
    filter.values.push(value)
  },
  [types.REMOVE_AGENTS_FILTER] (state, {key}) {
    let filter = state.agents.filters.find(el => key === el.key)
    let index = state.agents.filters.indexOf(filter)
    state.agents.filters.splice(index, 1)
  },
  [types.REMOVE_AGENTS_FILTER_VALUE] (state, {key, value}) {
    let filter = state.agents.filters.find(el => key === el.key)
    let filterValues = filter.values
    const index = filterValues.indexOf(value)
    filterValues.splice(index, 1)
  },
  [types.CLEAR_AGENTS_FILTER] (state, {key}) {
    let filter = state.agents.filters.find(el => key === el.key)
    filter.values.splice(0)
  },
  [types.CLEAR_CURRENT_AGENT] (state) {
    state.currentAgent = null
  },
  [types.SET_AGENT_SORT_PARAMS] (state, sortParams) {
    state.agents.sortParams = sortParams
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
