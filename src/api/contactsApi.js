import axios from 'axios'
import {CURRENT_URL, ngrok_url} from '../utils/Constants.js'
import Utils from '../utils/Utils.js'
import Contact from '../models/Contact.js'
const USE_FAKE_API = true
import store from '@/store/index'

export default {
  get (params) {
    // const URL = `${API_URL}customers`
    // const URL = USE_FAKE_API ? `${FAKE_API_URL}customers` : `${API_URL}customers`
    // params.Page = params.Page || 1
    // params.PageSize = params.PageSize || 30
    // params.state = params.state || "Active"
    // console.info('dart', params)
    // return axios.get(URL, {params: params})
    // .then(data => {
    //   let contacts = data.customers.map( el => {
    //     el.phone = el.phone_numbers && el.phone_numbers[0] ? el.phone_numbers[0] : null
    //     return el
    //   })
    //   return {contacts: contacts,
    //     meta: {Page: data.current_page, PageSize: data.current_page_total,
    //       TotalPages: data.total_pages, TotalFilteredRecords: data.total_filtered,
    //       TotalRecords: data.total_records
    //     }
    //   }
    // })
    let client_code = store.getters.user.client_code
    const URL = `${ngrok_url}farmers?client_codes[]=${client_code}`
    params.page = params.Page || 1
    params.limit = params.PageSize || 30
    //params.account_code = "gsma"
    // params.state = params.state || "Active"
    return axios.get(URL, {params: params})
    .then(data => {
      let contacts = data.farmers.map(el => {
        el.name = `${el.first_name} ${el.surname}`
    
        return el
      })
      return {contacts: contacts,
        meta: {Page: data.page,
          PageSize: data.current_page_total,
          TotalPages: data.pages_total,
          TotalFilteredRecords: data.records_filtered,
          TotalRecords: data.records_total
        }
      }
    })
  },
  findContact (id) {
    const URL = `${CURRENT_URL}farmers/${id}`
    return axios.get(URL)
    .then(el => {
      // el.phone = el.phone_numbers && el.phone_numbers[0] ? el.phone_numbers[0] : null
      return new Contact(el)
    })

    // return new Promise((resolve, reject) => {
    //   let contact = _contacts.find( (el) => {return el.id == id})
    //   resolve(new Contact(contact))
    // })
  },
  // findContact (id) {
  //   const URL = `${BASE_URL}Contact/${id}`
  //   return axios.get(URL)
  //   .then(response => {
  //     let data =  response.data
  //     console.error("ssh", data)
  //     return this.normalizeContact(data)
  //   })

  // },
  normalizeContact (contact) {
    contact.name = Utils.concatString([contact.title, contact.firstName, contact.lastName], ' ')
    contact.status = contact.isActive ? 'Active' : 'Inactive'
    return contact
  },
  normalizeContacts (contacts) {

  }

}
