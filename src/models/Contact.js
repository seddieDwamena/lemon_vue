import Utils from '../utils/Utils.js'

const fakeContact =  {
  "id": 2789,
  "first_name": "DASASD",
  "surname": "ASDASDAS",
  "other_names": "ASDASD",
  "age": null,
  "date_of_birth": "1998-09-01T00:00:00.000Z",
  "code": "RCLGARDZO0022789",
  "region": "Greater Accra Region",
  "region_id": 61,
  "reason": "",
  "is_archived": false,
  "created_at": "2018-08-22T13:08:04.625Z",
  "updated_at": "2018-08-22T13:10:07.214Z",
  "created_by": "Edwin Nyawoli",
  "updated_by": "Edwin Nyawoli",
  "contacts": [
    {
      "id": 4141,
      "active": true,
      "carrier": "unknown",
      "number": "0123123123",
      "ported": false,
      "verified": false,
      "type": "phone"
    }
  ],
  "addresses": [],
  "additional_info": {
    "photo": "iVBORw0KGgoAAAANSUhEUgAAAV4AAAFeCAIAAABCSeBNAAAAA3NCSVQICAjb4U/gAAAH20lEQVR4\nnO3dP24U2RrG4TrlEhgiSECESAR0wyYQ7Yw9sAmYzfnPMugmgBDJGWRIuOtMYGl0xX3v1fQpd02N\n9DyRA0pfvQ5+andCOT097Q5Xa91sNuv1uuHZQ52fn+92u2NfsWgKi9oseVF/vFcB/r2kAQikAQik\nAQiGcRwbHiul9H1/+8Ndv1LQ9pIHsWgiixosedHQ9k5934/jOI7j7bBjm+EXZ9FEFjVY8qLy8ePH\nhhvjOF5eXm6324ZnD3V2drZarY59xaIpLGqz5EVD25m/8jPPB6EZWLR8Fs3J15BAIA1AIA1AIA1A\nIA1AIA1AIA1AIA1AIA1AIA1AIA1AIA1AIA1AIA1AIA1AIA1AIA1AIA1AIA1AIA1AIA1AIA1AIA1A\nIA1AIA1AIA1AIA1AIA1AIA1AIA1AIA1AIA1AIA1AUE5PTxseq7VuNpvXr1+P43jn7/Sbi4uL7XZ7\n7CsWTWFRmyUvGkopDWdKKbXWm5ubtscbzs1wwqKJ52Y4YdHEc3//Hw+11oYbtdZSSt/P8fdIrbXt\nJQ+9YtGUWxa1XVnsIt81AIE0AIE0AIE0AIE0AIE0AIE0AIE0AIE0AIE0AIE0AIE0AIE0AIE0AIE0\nAIE0AIE0AIE0AIE0AIE0AIE0AIE0AIE0AIE0AIE0AIE0AIE0AIE0AIE0AIE0AMHQ/D+Fl1Lu9lX+\njxn+O/POomksarPYRUPzm83zi+u6rpQyz6/PomYWNVvsovLu3buGM+M4fv/+/efPnzMMe/z48cOH\nD499xaIpLGqz5EXDarVqOFNrvbi42G63M5R1s9m0veRBLJrCojZLXuRrSCCQBiCQBiCQBiCQBiCQ\nBiCQBiCQBiCQBiCQBiCQBiCQBiCQBiCQBiCQBiCQBiCQBiCQBiCQBiCQBiCQBiCQBiCQBiCQBiCQ\nBiCQBiCQBiCQBiCQBiCQBiCQBiCQBiAY2h4rpdz+UGu9u5f5J1m0fBbNaRjHseGx2yV9P8eHjlpr\n20seeqWzaMIti9qudEtdVO7fv99wpu/7N2/erFarGVadn59vt9tjX7FoCovaLHnR8NdHmoOM41hK\nmad23X987joeiyayqMGSF/kaEgikAQikAQikAQikAQikAQikAQikAQikAQikAQikAQikAQikAQik\nAQikAQikAQikAQikAQikAQikAQikAQikAQikAQikAQikAQikAQikAQikAQikAQikAQikAQikAQik\nAQjKH3/80fDYzc3N1dXVbrertd75O/3m7OxsvV4f+4pFU1jUZsmLhnEcG86cnJzUWvf7/cnJScPj\nB6m1tr3kQSyawqI2S140lFLaLpVS+n6Ov0dKKc0veeghi5oPWdR8aJmLGt9phs8/M7No+Syak68h\ngUAagEAagEAagEAagEAagEAagEAagEAagEAagEAagEAagEAagEAagEAagEAagEAagEAagEAagEAa\ngEAagEAagEAagEAagEAagEAagEAagEAagEAagGCY+PyS/6vfNhYtn0UzGKa8UynlDl/lf6m1zvaL\ns6iNRVMsc1FZr9dtl54+ffro0aO+P/qfJN++ffvx48exr3QWTWBRs8UuGr5+/dpwptb6/PnzFy9e\nNDx7qC9fvrS95EEsmsKiNkte5GtIIJAGIJAGIJAGIJAGIJAGIJAGIJAGIJAGIJAGIJAGIJAGIJAG\nIJAGIJAGIJAGIJAGIJAGIJAGIJAGIJAGIJAGIJAGIJAGIJAGIJAGIJAGIJAGIJAGIJAGIJAGIJAG\nIJAGIBhqrQ2P9X1fStnv930/R1zaXvIgFk1kUYMlLyofPnxou3F5ebnb7UopDY8f5O3bty9fvjz2\nFYumsKjNkhcNU96p1jpDWbuum+EXd8uiZhY1W+aiYc5j/woWLZ9FM/A1JBBIAxBIAxBIAxBIAxBI\nAxBIAxBIAxBIAxBIAxBIAxBIAxBIAxBIAxBIAxBIAxBIAxBIAxBIAxBIAxBIAxBIAxBIAxBIAxBI\nAxBIAxBIAxBIAxBIAxBIAxBIAxBIAxCU09PThsdqrZvNZr1e3/kL/bfz8/PdbnfsKxZNYVGbJS/y\nqQEIpAEIpAEIpAEIpAEIpAEIpAEIpAEIpAEIpAEIpAEIpAEIpAEIpAEIpAEIpAEIpAEIpAEIpAEI\npAEIpAEIpAEIpAEIpAEIpAEIpAEIpAEIpAEIpAEIpAEIpAEIpAEIpAEIhrbHSild143jePvDUc1w\norPoLm7Nc8WiKbcO+Pfv379vu/Tp06fr6+v9ft/2+N/36tWrZ8+eHftKZ9EEFjVb7KLhyZMnDWdq\nrb9+/bq+vp6hdvfu3Wt7yYNYNIVFbZa8yHcNQCANQCANQCANQCANQCANQCANQCANQCANQCANQCAN\nQCANQCANQCANQCANQCANQCANQCANQCANQCANQCANQCANQCANQCANQCANQCANQCANQCANQCANQCAN\nQCANQCANQCANQNCYhr7v+74/OTm527f5B1m0fBbNqTx48KDhsVrrZrNZrVb7/f7O3+k3V1dXnz9/\nPvYVi6awqM2SFw3jODacqbV2XVdKmSd4bS95EIsmsqjBkhcNpZS2M7XW22EzaH7Jg1g0hUVtFrvI\n15BAIA1AIA1AIA1AIA1AIA1AIA1AIA1AIA1AIA1AIA1AIA1AIA1AIA1AIA1AIA1AIA1AIA1AIA1A\nIA1AIA1AIA1A8CfpuJo7dSwgLQAAAABJRU5ErkJggg==\n",
    "id_type": "Driver's Licence",
    "id_number": "SADSADADASDSAS",
    "front_of_id": "iVBORw0KGgoAAAANSUhEUgAAAV4AAAFeCAIAAABCSeBNAAAAA3NCSVQICAjb4U/gAAANMElEQVR4\nnO3dz28cZx3H8e/3eWbtbUQCUZGQUAQcOPDj0l567AERxJUekHoAcUNCNumh+TcKqVMQ/wCHCglx\nROof0DvqEQVVQA9UCDVpbO/uPF8Ou/4Vf5x6ZjOzY8/7ddi60cw+z9f2vnc9cWy/f/++NfTgwYOU\nUtOzWtjd3e1hFWOiNTBRawOfqI+dAbhySAMAgTQAEEgDAIE0ABBIAwCBNAAQSAMAgTQAEEgDAIE0\nABBIAwCBNAAQSAMAoWpxjrtHxAvfygYx0fAxUc8qd290gru7eymlow09s1Y/qzDROmv1swoTrbNW\nm7Nef/31rtdorYemMtGamKiFKzGRT6fTRmtExL1791qs1MLe3l7XSzDRmpiohSsxEZchAQikAYBA\nGgAIpAGAQBoACKQBgEAaAAikAYBAGgAIpAGAQBoACKQBgEAaAAikAYBAGgAIpAGAQBoACKQBgEAa\nAAikAYBAGgAIpAGAQBoACKQBgEAaAAikAYBAGgAIpAGAQBoACKQBgEAaAAikAYBAGgAIVUQ0OsHd\nO9rKeU331g4TrYOJ2hn+RP722283ONo9Ih4+fNhipRZ2dna6XoKJ1sRELVyJiaqm9XL3lFJd1y0W\na6qfsjLRmmv1swoTrbNWi7OqFucsFouUrtVFCiYaPibq2XB3BmCDSAMAgTQAEEgDAIE0ABBIAwCB\nNAAQSAMAgTQAEEgDAIE0ABBIAwCBNAAQSAMAgTQAEEgDAIE0ABBIAwCBNAAQSAMAgTQAEEgDAIE0\nABBIAwCBNAAQSAMAgTQAEEgDAIE0ABBIAwCBNAAQSAMAwafT6eWPjoiIuHfvXncbOm1vb6/rJS6a\nqKo96tnX9j99Or39pfncw4plMzMPCw8Ps2RWjm79/B2fPiCHmVkx3/vD78MszNyiw6k8/fqXv0ol\nzCI8ivsz+zm74TC78IAqDh7d/ur00Ocp3BYpcp3O7HyDH6OOMNFS9cL3cT3MqoPvPP38m+//+aWY\nhs8Pq8j1xM3Cws3DwlaPt2duTf1hFDPzCEu/e/krYebLF2urezp3a7Y698IDlvd84T14RHr/jx7L\nR3j4uf08Z8MR7qf+5PNq8q8f/PDgm19P4WF2UPl23WXUMBikQcvzG9XT2a3JNA4XuS6TOpnPjx6t\n6gF55lXAswek1QERq+MWHW7dzd0jioeFHcfh4g2fPSDMw8OPDkglqnT4dLK4cTDJJZ30BNcdabjA\nZFblwzTbrz3Pc7VIKUX7Z8vwEsvH7Oo+vMvHl5tZeMurSKsXEkf7m2cvHtuHJSw92a4nXTYNg0Ia\ntNrT3Kv9vL2w6qVFmS5q8/r0AY06EZaWD7qjV/ARfvxqP8Xqa/sv/sLk0gesHtvLEqUme42z0arK\nxKIKm0R48eK8ZhgN0qDlMDfLUXIski9qN2v7PHzK6Rcey3tbPXiXb5+6feZ/z99+wQF+6pCyxsO5\nuBXPtXt2my48h9fOtYZR4C8vteWjqawuOaYX0YUrafn3MR4WVmy9yuBqGelnPIDnIw0ABNIAQCAN\n+AJh4V6i2+/fxOCQBjyPe/D3leNEGgAIpAGAQBoACKQBgEAaAAikAYBAGgAIpAGAQBoACKQBgEAa\nAAikAYBAGgAIpAGAQBoACKQBgEAaAAikAYBAGgAIpEErJ2/y81IxRqRB8zNvp3LhgcD1RBo0P/p1\nlMA4NUuDu6eUvNNfAd+viyaqI2b1IoI8eCklpbzJHYzms25Q/O7du41OKKU8fvy4n6lu3brVwyp6\nosq+/+njn//nn7muJhFlrF9S7Gd/+K1v/3srW50i1ank8DPvik1+jLrBREvVK6+80vScvb29Fiu1\nsLu7289C5ycqXur9mb/8ZTczqy18tL+p5eOPP/7b4/+lqMLrFM+mYYMfo44w0VKbaw2lXLen0PMT\npeUvjXd3s/Cw0YZhMMbwWTcoXIYEIJAGAAJpACCQBgACaQAgkAYAAmkAIJAGAAJpACCQBgACaQAg\nkAYAAmkAIJAGAAJpACCQBgACaQAgkAYAAmkAIJAGAAJpACCQBgACaQAgkAYAAmkAIJAGAAJpACCQ\nBgACaQAgkIZLCDeLTW8C6BVp0NzMTnqQV/8PjAZp0IqZ2akg8KIBI1M1OjoiIsL9+jyHXjSRJy9m\nVkqxNO58urtFmJuF+0YSOZ7PukHx6XR6+aMjopTy1ltvlVK629Ox9957r+slLpooZ//ufx9/7y9/\nms0iW5iVcX5N8TT5X3/04/jGHVt4SSWVFH7mHbXBj1FHmGip2asGM8s5m1m6Rk+lcqJSL+az2f7T\n/TyZRpRRZsHMLKWUPBV3T8lSJEuxiffFSD7rBqXxzvrpXJ/kRDnnSVWl7UmKSGbjfMlgZhHF3Uop\nYZu84DKSz7pBGW60AGwQaQAgkAYAAmkAIJAGAAJpACCQBgACaQAgkAYAAmnA80S4hZejzxP+Aep4\nkAZc2li/W3ycSAMAgTQAEEgDAKHxz2sYiYO88C17vH3zdlnsV6l2dzv+V7TLnyLrZsUsHb19/grd\n8QHFLIVZnfyz/f1iHu4ex2ctDzi+z2Nx6g/P3154QLLwur5982bUtZmZh8eZnVyw1dVEcfbZ4rPq\n5tZi/iTVua5KlMh1Nd4fXjEupEHLhy/9fXvrHz/9yWwrcplM6lSn9pfnV6e6/ea3D44enF1d7C9u\nKVc7P/tFldIyCc0uH57dV7WYLaZpWqoU5mZb85hl0jAKpEErW08Oi09sa9/N3FOy4ut+8eVmT7cq\nswhL4WHhJm+PXxS0OyB5TulJ9ipnMy9uYWEXvLCRmzSz4xcXcWOSFmXr0OdVzKqwuvBF6EiQBm1r\nsW3mB1ZZKpNSpyin0xBmKaz46vb80+i5A9zMY/WA8xTukVbHydvTd9TwgKhLLOaTlC3CzHOER4Sf\n7Ef+BLezG17+fNgwMzuMsHQ4cbdyYx6LlHjNMBKkQQtzD9uKw7Co3RbJn3lI1X5yK5+NTx9gVixS\nuC1/MnNJq2fyjrZulsJPvmwJ93P7ec6G0+o+zM2seKnKIpKlYpM6LTyZD/0nl+GFIA0XySXVxbKb\nheWISGs8mMOyuXm4h5u5d/xNhSWWGTv62fDNnujPbM4t3GJSe3E/qKzOUVGGcSANF/CT51d/3nPt\n5e5s9Z+wrqtgFhEnlznXfvWfwovn1T25VYVvlR4LLikBEEgDAIE0ABBIAwCBNAAQSAMAgTQAEEgD\nAIE0ABCqiGbf3+buEeHex7+yabq3dphoHUzUzvAnqppurq7rnHMpfXwnfT/vOCZaBxO1M/yJ/P79\n+03Peffdd/t59+3u7vawijHRGpiotYFP1OZaQz+vuPrERMPHRD3jMiQAgTQAEEgDAIE0ABBIAwCB\nNAAQSAMAgTQAEEgDAIE0ABBIAwCBNAAQSAMAgTQAEEgDAIE0ABBIAwCBNAAQSAMAgTQAEEgDAIE0\nABBIAwCBNAAQSAMAgTQAEEgDAIE0ABBIAwCBNAAQSAMAgTQAEKpGR0eEmaWUlm9cA0w0fEy0Ef7q\nq682OqGUcufOnX5G+uSTT3pYhYnWwUTtDH+i6qOPPmp0grvfvXvX3Vss1tQHH3zQwypMtA4mamf4\nE1UpNbjcEBER0c88ZtZob+0w0ZqYqIUrMRGXIQEIpAGAQBoACKQBgEAaAAikAYBAGgAIpAGAQBoA\nCKQBgEAaAAikAYBAGgAIpAGAQBoACKQBgEAaAAikAYBAGgAIpAGAQBoACKQBgEAaAAikAYBAGgAI\npAGAQBoACKQBgEAaAAikAYBAGgAIpAGA0CwN7m5mpZRuNrMBTDR8TLQR1c7OTqMTUkrvvPPOZDLp\naEOnNd1bO0y0DiZqZ/gTVSk1eOEQEcvg1XXdYrGmGu2tHSZaExO1cCUmqiKi0QmllJRSD+8+M2u6\nt3aYaB1M1M7wJ+IyJACBNAAQSAMAgTQAEEgDAIE0ABBIAwCBNAAQSAMAgTQAEEgDAIE0ABBIAwCB\nNAAQSAMAgTQAEEgDAIE0ABBIAwCBNAAQSAMAgTQAEEgDAIE0ABBIAwCBNAAQSAMAgTQAEEgDAIE0\nABCqRr9g+/hgd+9mP3q5HpZgojWX62EJJlpzuUaqRke7e0TknHt49/WDiYaPiTbCp9Pp5Y+OCHff\n2dnpp3Z7e3tdL8FEa2KiFq7ERI2vNZRS+pmnN0w0fEzUPy5DAhBIAwCBNAAQSAMAgTQAEEgDAIE0\nABBIAwCBNAAQSAMAgTQAEEgDAIE0ABBIAwCBNAAQSAMAgTQAEEgDAIE0ABBIAwCBNAAQSAMAgTQA\nEEgDAIE0ABBIAwCBNAAQSAMAgTQAEEgDAIE0ABBIAwChTRpKKS98H5vFRMPHRD3zN9988/JHR0RV\nVR9++GFEdLenY6+99lrXSzDRmpiohSsxUXXnzp3LH72c5NGjRznnFos19cYbb3S9BBOtiYlauBIT\nVY2OdnczyzmndE0uUjDR8DHRRgx3ZwA2iDQAEEgDAIE0ABBIAwCBNAAQSAMAgTQAEEgDAIE0ABBI\nAwCBNAAQ/g9Skx0vgXu1qAAAAABJRU5ErkJggg==\n",
    "has_children": true,
    "home_landmark": "DSAASSA",
    "chemicals_used": [
      "Fertilizers",
      "Fungicides",
      "Insecticides"
    ],
    "ages_of_children": [
      "14",
      "12",
      "12",
      "13"
    ],
    "home_coordinates": {
      "id": 0,
      "label": "Home Address",
      "latitude": 34.1786998,
      "longitude": -86.6154153
    },
    "number_of_spouses": "1",
    "other_enterprises": [
      "Animal husbandry",
      "Food processing"
    ],
    "farm_bounds": []
  },
  "status": "unapproved",
  "neighbours": [
    {
      "id": 35281,
      "contact_details_id": null,
      "created_at": "2018-08-22T13:08:43.200Z",
      "date_of_birth": null,
      "economic_details_id": null,
      "first_name": "Baba",
      "gender": null,
      "marital_details_id": null,
      "nationality_id": null,
      "next_of_kin_id": null,
      "other_names": "",
      "place_of_birth": null,
      "surname": "God",
      "title": null,
      "age": null,
      "region_id": null,
      "town": "",
      "data": {},
      "phone_number": null
    }
  ],
  "geo_coordinate": {
    "lat": "34.1786998",
    "lng": "-86.6154153"
  },
  "farms": [
    {
      "id": 17,
      "size": "213.0",
      "data": {
        "crops_grown": [
          "Plantain / Banana",
          "Cocoa"
        ]
      },
      "farmer_id": 2789,
      "created_at": "2018-08-22T13:08:36.639Z",
      "landowner_id": null,
      "caretakers": [
        {
            "id": 34621,
            "contact_details_id": 2827,
            "created_at": "2018-08-09T21:26:16.898Z",
            "date_of_birth": "1995-03-01T00:00:00.000Z",
            "economic_details_id": null,
            "first_name": "Teeee",
            "gender": null,
            "marital_details_id": null,
            "nationality_id": null,
            "next_of_kin_id": null,
            "other_names": "Kwaku",
            "place_of_birth": null,
            "surname": "Phlow",
            "title": null,
            "age": 23,
            "region_id": null,
            "town": null,
            "data": {},
            "phone_number": ""
        }
      ],
      "landowner": {
        "id": 34621,
        "contact_details_id": 2827,
        "created_at": "2018-08-09T21:26:16.898Z",
        "date_of_birth": "1995-03-01T00:00:00.000Z",
        "economic_details_id": null,
        "first_name": "Teeee",
        "gender": null,
        "marital_details_id": null,
        "nationality_id": null,
        "next_of_kin_id": null,
        "other_names": "Kwaku",
        "place_of_birth": null,
        "surname": "Phlow",
        "title": null,
        "age": 23,
        "region_id": null,
        "town": "Ejisu",
        "data": {},
        "phone_number": ""
      },
      "farm_bounds": [
        {
          "lat": "34.1786998",
          "lng": "-86.6154153"
        },
        {
          "lat": "34.1786998",
          "lng": "-86.6154153"
        },
        {
          "lat": "34.1786998",
          "lng": "-86.6154153"
        }
      ],
      "additional_data": {
        "crops_grown": [
          "Plantain / Banana",
          "Cocoa"
        ]
      }
    },
    {
      "id": 18,
      "size": "123.0",
      "data": {
        "crops_grown": [
          "Cocoyam"
        ]
      },
      "farmer_id": 2789,
      "created_at": "2018-08-22T13:08:37.456Z",
      "landowner_id": null,
      "farm_bounds": [
        {
          "lat": "34.1786998",
          "lng": "-86.6154153"
        },
        {
          "lat": "34.1786998",
          "lng": "-86.6154153"
        },
        {
          "lat": "34.1786998",
          "lng": "-86.6154153"
        }
      ],
      "additional_data": {
        "crops_grown": [
          "Cocoyam"
        ]
      }
    }
  ],
  "society": {
    "id": 2,
    "name": "A test group",
    "client_id": 4,
    "district_id": 2,
    "created_at": "2018-08-09T11:29:03.175Z",
    "is_active": true,
    "agent_contract_id": 289,
    "district": "Dzowulu",
    "sector": "Accra",
    "farmer_count": 3,
    "assigned_agent": "Benjamin FRIMPONG",
    "assigned_agent_id": 289
  }
}

// properties
let Contact = function(obj, isLean) {
  // obj = fakeContact
  isLean = isLean || false
  this.isLean = isLean
  let contact = obj ? Utils.cloneObject(obj) : {}
  contact.name = `${contact.first_name} ${contact.surname}`
  contact.contacts = contact.contacts || []
  let phone = contact.contacts.find(el => el.hasOwnProperty("number"))
  contact.msisdn = phone ? `${phone.number}` : ''
  contact.unique_id =`${contact.code}`  
  // if(isLean){
  //   return
  // }
  contact.neighbours = contact.neighbours || []
  contact.additional_info = contact.additional_info || {}
  contact.other_enterprises = contact.additional_info.other_enterprises || []
  // delete contact["additional_info"]    
  let additional_info = contact.additional_info 
  additional_info.photo = additional_info.photo || null
  additional_info.front_of_id = additional_info.front_of_id || null
  additional_info.back_of_id = additional_info.back_of_id || null
  additional_info.id_number = additional_info.id_number || null
  additional_info.has_children = additional_info.has_children || false
  additional_info.id_type = additional_info.id_type || null
  additional_info.home_landmark = additional_info.home_landmark || null
  additional_info.number_of_spouses = additional_info.number_of_spouses || null
  additional_info.ages_of_children = additional_info.ages_of_children || []
  additional_info.chemicals_used = additional_info.chemicals_used || []
  additional_info.other_enterprises = additional_info.other_enterprises || []
  additional_info.sales = additional_info.sales || []
  contact.photo = additional_info.photo
  contact.farms = contact.farms || []
  if(Array.isArray(contact.addresses)){
    contact.address = contact.addresses[0]
  }else{
    contact.address = {}
  }

  // landowners
  if(Utils.present(contact.landowners)){
    contact.landowners.data =  contact.landowners.data || {}
  }
  // only the ages are sent to us 
  contact.children = []
  if(contact.additional_info && Array.isArray(contact.additional_info.ages_of_children)){
    contact.children = contact.additional_info.ages_of_children.map(el => {
      return {name: "", age: el.age}
    })
  }else{
    contact.children = []
  }
  contact.contacts.sort((a, b) => a.primary == true)
  contact.id_type = contact.additional_info.id_type
  if(typeof additional_info.home_coordinates != 'object'){
    additional_info.home_coordinates = {}
  }
  contact.address = {
    house_number: "",
    street: "",
    description: additional_info.home_landmark,
    label: additional_info.home_coordinates.label
  }
  contact.society = contact.society || {}
  if(Array.isArray(contact.farms)){
    let farms = contact.farms
    for(let i = 0; i < farms.length; i++){
      let farm = farms[i]
      farm.data = farm.data || {} 
      farm.data.crops_grown = farm.data.crops_grown || []
      // farm.amount_sold_last_season = farm.amount_sold_last_season || {}
      // farm.amount_sold_last_two = farm.amount_sold_last_two || {}
      // farm.amount_sold_last_three = farm.amount_sold_last_three || {}
      // farm.amount_sold_to_rc = farm.amount_sold_to_rc || {}
      // farm.neighbouring_farmers = farm.neighbouring_farmers || []
      farm.farm_bounds = farm.farm_bounds || [] 
      farm.caretakers = farm.caretakers || [] 
    }
  }
  Object.assign(this, contact)
}





// methods
Contact.prototype.getViewModel = function () {
  let contact = this.contact
  

  let farmMenuItems = this.menuItems.filter(e => e.name.startsWith("Farm "))
  farmMenuItems.forEach(f => this.menuItems.splice(this.menuItems.findIndex(e => e.name === f.name),1));
  if(Array.isArray(contact.farms)){
    let farms = contact.farms
    for(let i = 0; i < farms.length; i++){
      let farm = farms[i]
      farm.data = farm.data || {} 
      farm.data.crops_grown = farm.data.crops_grown || []
      farm.amount_sold_last_season = farm.amount_sold_last_season || {}
      farm.amount_sold_last_two = farm.amount_sold_last_two || {}
      farm.amount_sold_last_three = farm.amount_sold_last_three || {}
      farm.amount_sold_to_rc = farm.amount_sold_to_rc || {}
      farm.neighbouring_farmers = farm.neighbouring_farmers || []
      farm.farm_bounds = farm.farm_bounds || [] 
      farm.caretakers = farm.caretakers || [] 
      let farmMenuItem = {name: farms.length > 1 ?`Farm ${i + 1} Data` : "Farm Data",
        children: [{name: "Trade History"},
            {name: "Neighbouring Farmers"}
        ]
      }
      if(Utils.present(farm.landowner)){
        farmMenuItem.children.push({name: "Landowner"})
      }
      farmMenuItem.children.push({name: "Caretakers"})
      this.menuItems.push(farmMenuItem)
    }
    if(farms.length == 0){
      this.menuItems.push({name: "Farm Data"})
    }
  }
}

const getSaveModel = function (viewModel) {
  let model = {} 
  model.first_name = viewModel.first_name
  model.surname = viewModel.surname
  model.other_names = viewModel.other_names
  model.date_of_birth = viewModel.date_of_birth
  // model.other_names = viewModel.other_names
  model.contact_info = viewModel.contacts.map(el => {
    el.type = "phone"
    return el
  })
  if(model.contact_info[0]){
    model.contact_info[0].primary = true
  }
  model.region_id = viewModel.region_id 
  model.society_id = viewModel.society.id 
  model.additional_info = viewModel.additional_info
  
  // let address = contact.address || {}
  // model.addresses = [address]
  return model
}
Contact.getSaveModel = getSaveModel
export default Contact


// contadct:{
//   "id": 2778,
//   "first_name": "Teeee",
//   "surname": "Phlow",
//   "other_names": "Kwaku",
//   "age": 23,
//   "date_of_birth": null,
//   "code": "RCLGAR0220032778",
//   "region": "Greater Acrra Region",
//   "region_id": 61,
//   "is_archived": false,
//   "contacts": [],
//   "addresses": [],
//   "additional_info": {
//       "other_enterprises": [],
//       "farm_bounds": []
//   },
//   "status": "Not Approved",
//   "farms": [
//       {
//           "id": 2,
//           "size": "2.77778",
//           "data": "{}",
//           "farmer_id": 2778,
//           "created_at": "2018-08-10T04:26:45.994Z",
//           "farm_bounds": [],
//           landowner: {
//             first_name: "kwame"
//           },
//           neighbouring_farmers: [
//             {name: "kwaku", isRcf: true}
//           ]
//       },
//       {
//           "id": 3,
//           "size": "2.77778",
//           "data": "{}",
//           "farmer_id": 2778,
//           "created_at": "2018-08-10T04:28:18.414Z",
//           "farm_bounds": [
//               {
//                   "lat": "1.4",
//                   "lng": "1.4"
//               },
//               {
//                   "lat": "1.3",
//                   "lng": "1.3"
//               },
//               {
//                   "lat": "1.2",
//                   "lng": "1.2"
//               }
//           ]
//       },
//       {
//           "id": 4,
//           "size": "2.77778",
//           "data": "{}",
//           "farmer_id": 2778,
//           "created_at": "2018-08-10T04:29:09.740Z",
//           "farm_bounds": [
//               {
//                   "lat": "1.4",
//                   "lng": "1.4"
//               },
//               {
//                   "lat": "1.3",
//                   "lng": "1.3"
//               },
//               {
//                   "lat": "1.2",
//                   "lng": "1.2"
//               }
//           ]
//       },
//       {
//           "id": 5,
//           "size": "2.77778",
//           "data": "{}",
//           "farmer_id": 2778,
//           "created_at": "2018-08-10T04:30:59.231Z",
//           "farm_bounds": [
//               {
//                   "lat": "1.4",
//                   "lng": "1.4"
//               },
//               {
//                   "lat": "1.3",
//                   "lng": "1.3"
//               },
//               {
//                   "lat": "1.2",
//                   "lng": "1.2"
//               }
//           ]
//       },
//       {
//           "id": 6,
//           "size": "2.77778",
//           "data": "{}",
//           "farmer_id": 2778,
//           "created_at": "2018-08-10T04:31:45.679Z",
//           "landowner": {
//               "id": 34621,
//               "contact_details_id": 2827,
//               "created_at": "2018-08-09T21:26:16.898Z",
//               "date_of_birth": null,
//               "economic_details_id": null,
//               "first_name": "Teeee",
//               "gender": null,
//               "marital_details_id": null,
//               "nationality_id": null,
//               "next_of_kin_id": null,
//               "other_names": "Kwaku",
//               "place_of_birth": null,
//               "surname": "Phlow",
//               "title": null,
//               "age": 23,
//               "region_id": null,
//               "town_id": null,
//               "is_landowner": true,
//               "is_caretaker": true
//           },
//           "caretakers": [
//               {
//                   "id": 34621,
//                   "contact_details_id": 2827,
//                   "created_at": "2018-08-09T21:26:16.898Z",
//                   "date_of_birth": null,
//                   "economic_details_id": null,
//                   "first_name": "Teeee",
//                   "gender": null,
//                   "marital_details_id": null,
//                   "nationality_id": null,
//                   "next_of_kin_id": null,
//                   "other_names": "Kwaku",
//                   "place_of_birth": null,
//                   "surname": "Phlow",
//                   "title": null,
//                   "age": 23,
//                   "region_id": null,
//                   "town_id": null,
//                   "is_landowner": true,
//                   "is_caretaker": true
//               }
//           ],
//           "farm_bounds": [
//               {
//                   "lat": "1.4",
//                   "lng": "1.4"
//               },
//               {
//                   "lat": "1.3",
//                   "lng": "1.3"
//               },
//               {
//                   "lat": "1.2",
//                   "lng": "1.2"
//               }
//           ]
//       },
//       {
//           "id": 7,
//           "size": "2.77778",
//           "data": "{}",
//           "farmer_id": 2778,
//           "created_at": "2018-08-10T04:37:15.434Z",
//           "landowner": {
//               "id": 34621,
//               "contact_details_id": 2827,
//               "created_at": "2018-08-09T21:26:16.898Z",
//               "date_of_birth": null,
//               "economic_details_id": null,
//               "first_name": "Teeee",
//               "gender": null,
//               "marital_details_id": null,
//               "nationality_id": null,
//               "next_of_kin_id": null,
//               "other_names": "Kwaku",
//               "place_of_birth": null,
//               "surname": "Phlow",
//               "title": null,
//               "age": 23,
//               "region_id": null,
//               "town_id": null,
//               "is_landowner": true,
//               "is_caretaker": true
//           },
//           "caretakers": [
//               {
//                   "id": 34621,
//                   "contact_details_id": 2827,
//                   "created_at": "2018-08-09T21:26:16.898Z",
//                   "date_of_birth": null,
//                   "economic_details_id": null,
//                   "first_name": "Teeee",
//                   "gender": null,
//                   "marital_details_id": null,
//                   "nationality_id": null,
//                   "next_of_kin_id": null,
//                   "other_names": "Kwaku",
//                   "place_of_birth": null,
//                   "surname": "Phlow",
//                   "title": null,
//                   "age": 23,
//                   "region_id": null,
//                   "town_id": null,
//                   "is_landowner": true,
//                   "is_caretaker": true
//               }
//           ],
//           "farm_bounds": [
//               {
//                   "lat": "1.4",
//                   "lng": "1.4"
//               },
//               {
//                   "lat": "1.3",
//                   "lng": "1.3"
//               },
//               {
//                   "lat": "1.2",
//                   "lng": "1.2"
//               }
//           ]
//       },
//       {
//           "id": 8,
//           "size": "2.77778",
//           "data": "{}",
//           "farmer_id": 2778,
//           "created_at": "2018-08-10T04:38:16.368Z",
//           "landowner": {
//               "id": 34621,
//               "contact_details_id": 2827,
//               "created_at": "2018-08-09T21:26:16.898Z",
//               "date_of_birth": null,
//               "economic_details_id": null,
//               "first_name": "Teeee",
//               "gender": null,
//               "marital_details_id": null,
//               "nationality_id": null,
//               "next_of_kin_id": null,
//               "other_names": "Kwaku",
//               "place_of_birth": null,
//               "surname": "Phlow",
//               "title": null,
//               "age": 23,
//               "region_id": null,
//               "town_id": null,
//               "is_landowner": true,
//               "is_caretaker": true
//           },
//           "caretakers": [
//               {
//                   "id": 34621,
//                   "contact_details_id": 2827,
//                   "created_at": "2018-08-09T21:26:16.898Z",
//                   "date_of_birth": null,
//                   "economic_details_id": null,
//                   "first_name": "Teeee",
//                   "gender": null,
//                   "marital_details_id": null,
//                   "nationality_id": null,
//                   "next_of_kin_id": null,
//                   "other_names": "Kwaku",
//                   "place_of_birth": null,
//                   "surname": "Phlow",
//                   "title": null,
//                   "age": 23,
//                   "region_id": null,
//                   "town_id": null,
//                   "is_landowner": true,
//                   "is_caretaker": true
//               }
//           ],
//           "farm_bounds": [
//               {
//                   "lat": "1.4",
//                   "lng": "1.4"
//               },
//               {
//                   "lat": "1.3",
//                   "lng": "1.3"
//               },
//               {
//                   "lat": "1.2",
//                   "lng": "1.2"
//               }
//           ]
//       },
//       {
//           "id": 9,
//           "size": "2.77778",
//           "data": "{}",
//           "farmer_id": 2778,
//           "created_at": "2018-08-10T04:50:36.207Z",
//           "landowner": {
//               "id": 34621,
//               "contact_details_id": 2827,
//               "created_at": "2018-08-09T21:26:16.898Z",
//               "date_of_birth": null,
//               "economic_details_id": null,
//               "first_name": "Teeee",
//               "gender": null,
//               "marital_details_id": null,
//               "nationality_id": null,
//               "next_of_kin_id": null,
//               "other_names": "Kwaku",
//               "place_of_birth": null,
//               "surname": "Phlow",
//               "title": null,
//               "age": 23,
//               "region_id": null,
//               "town_id": null,
//               "is_landowner": true,
//               "is_caretaker": true
//           },
//           "caretakers": [
//               {
//                   "id": 34621,
//                   "contact_details_id": 2827,
//                   "created_at": "2018-08-09T21:26:16.898Z",
//                   "date_of_birth": null,
//                   "economic_details_id": null,
//                   "first_name": "Teeee",
//                   "gender": null,
//                   "marital_details_id": null,
//                   "nationality_id": null,
//                   "next_of_kin_id": null,
//                   "other_names": "Kwaku",
//                   "place_of_birth": null,
//                   "surname": "Phlow",
//                   "title": null,
//                   "age": 23,
//                   "region_id": null,
//                   "town_id": null,
//                   "is_landowner": true,
//                   "is_caretaker": true
//               }
//           ],
//           "farm_bounds": [
//               {
//                   "lat": "1.4",
//                   "lng": "1.4"
//               },
//               {
//                   "lat": "1.3",
//                   "lng": "1.3"
//               },
//               {
//                   "lat": "1.2",
//                   "lng": "1.2"
//               }
//           ]
//       },
//       {
//           "id": 10,
//           "size": "2.77778",
//           "data": "{}",
//           "farmer_id": 2778,
//           "created_at": "2018-08-10T04:51:03.107Z",
//           "landowner": {
//               "id": 34621,
//               "contact_details_id": 2827,
//               "created_at": "2018-08-09T21:26:16.898Z",
//               "date_of_birth": null,
//               "economic_details_id": null,
//               "first_name": "Teeee",
//               "gender": null,
//               "marital_details_id": null,
//               "nationality_id": null,
//               "next_of_kin_id": null,
//               "other_names": "Kwaku",
//               "place_of_birth": null,
//               "surname": "Phlow",
//               "title": null,
//               "age": 23,
//               "region_id": null,
//               "town_id": null,
//               "is_landowner": true,
//               "is_caretaker": true
//           },
//           "caretakers": [
//               {
//                   "id": 34621,
//                   "contact_details_id": 2827,
//                   "created_at": "2018-08-09T21:26:16.898Z",
//                   "date_of_birth": null,
//                   "economic_details_id": null,
//                   "first_name": "Teeee",
//                   "gender": null,
//                   "marital_details_id": null,
//                   "nationality_id": null,
//                   "next_of_kin_id": null,
//                   "other_names": "Kwaku",
//                   "place_of_birth": null,
//                   "surname": "Phlow",
//                   "title": null,
//                   "age": 23,
//                   "region_id": null,
//                   "town_id": null,
//                   "is_landowner": true,
//                   "is_caretaker": true
//               }
//           ],
//           "farm_bounds": [
//               {
//                   "lat": "1.4",
//                   "lng": "1.4"
//               },
//               {
//                   "lat": "1.3",
//                   "lng": "1.3"
//               },
//               {
//                   "lat": "1.2",
//                   "lng": "1.2"
//               }
//           ]
//       },
//       {
//           "id": 12,
//           "size": "2.77778",
//           "data": "{}",
//           "farmer_id": 2778,
//           "created_at": "2018-08-10T04:56:38.046Z",
//           "farm_bounds": [
//               {
//                   "lat": "1.4",
//                   "lng": "1.4"
//               },
//               {
//                   "lat": "1.3",
//                   "lng": "1.3"
//               },
//               {
//                   "lat": "1.2",
//                   "lng": "1.2"
//               }
//           ]
//       },
//       {
//           "id": 13,
//           "size": "2.77778",
//           "data": "{}",
//           "farmer_id": 2778,
//           "created_at": "2018-08-10T04:56:59.150Z",
//           "farm_bounds": [
//               {
//                   "lat": "1.4",
//                   "lng": "1.4"
//               },
//               {
//                   "lat": "1.3",
//                   "lng": "1.3"
//               },
//               {
//                   "lat": "1.2",
//                   "lng": "1.2"
//               }
//           ]
//       },
//       {
//           "id": 14,
//           "size": "2.77778",
//           "data": "{}",
//           "farmer_id": 2778,
//           "created_at": "2018-08-10T04:58:02.967Z",
//           "farm_bounds": [
//               {
//                   "lat": "1.4",
//                   "lng": "1.4"
//               },
//               {
//                   "lat": "1.3",
//                   "lng": "1.3"
//               },
//               {
//                   "lat": "1.2",
//                   "lng": "1.2"
//               }
//           ]
//       },
//       {
//           "id": 15,
//           "size": "2.77778",
//           "data": "{}",
//           "farmer_id": 2778,
//           "created_at": "2018-08-10T04:58:12.383Z",
//           "farm_bounds": [
//               {
//                   "lat": "1.4",
//                   "lng": "1.4"
//               },
//               {
//                   "lat": "1.3",
//                   "lng": "1.3"
//               },
//               {
//                   "lat": "1.2",
//                   "lng": "1.2"
//               }
//           ]
//       },
//       {
//           "id": 19,
//           "size": "2.77778",
//           "data": "{}",
//           "farmer_id": 2778,
//           "created_at": "2018-08-10T05:00:21.592Z",
//           "farm_bounds": [
//               {
//                   "lat": "1.4",
//                   "lng": "1.4"
//               },
//               {
//                   "lat": "1.3",
//                   "lng": "1.3"
//               },
//               {
//                   "lat": "1.2",
//                   "lng": "1.2"
//               }
//           ]
//       },
//       {
//           "id": 20,
//           "size": "2.77778",
//           "data": "{}",
//           "farmer_id": 2778,
//           "created_at": "2018-08-10T05:16:47.094Z",
//           "farm_bounds": [
//               {
//                   "lat": "1.4",
//                   "lng": "1.4"
//               },
//               {
//                   "lat": "1.3",
//                   "lng": "1.3"
//               },
//               {
//                   "lat": "1.2",
//                   "lng": "1.2"
//               }
//           ]
//       },
//       {
//           "id": 30,
//           "size": "2.77778",
//           "data": "{}",
//           "farmer_id": 2778,
//           "created_at": "2018-08-10T05:33:02.529Z",
//           "landowner": {
//               "id": 34621,
//               "contact_details_id": 2827,
//               "created_at": "2018-08-09T21:26:16.898Z",
//               "date_of_birth": null,
//               "economic_details_id": null,
//               "first_name": "Teeee",
//               "gender": null,
//               "marital_details_id": null,
//               "nationality_id": null,
//               "next_of_kin_id": null,
//               "other_names": "Kwaku",
//               "place_of_birth": null,
//               "surname": "Phlow",
//               "title": null,
//               "age": 23,
//               "region_id": null,
//               "town_id": null,
//               "is_landowner": true,
//               "is_caretaker": true
//           },
//           "caretakers": [
//               {
//                   "id": 34621,
//                   "contact_details_id": 2827,
//                   "created_at": "2018-08-09T21:26:16.898Z",
//                   "date_of_birth": null,
//                   "economic_details_id": null,
//                   "first_name": "Teeee",
//                   "gender": null,
//                   "marital_details_id": null,
//                   "nationality_id": null,
//                   "next_of_kin_id": null,
//                   "other_names": "Kwaku",
//                   "place_of_birth": null,
//                   "surname": "Phlow",
//                   "title": null,
//                   "age": 23,
//                   "region_id": null,
//                   "town_id": null,
//                   "is_landowner": true,
//                   "is_caretaker": true
//               }
//           ],
//           "farm_bounds": [
//               {
//                   "lat": "1.4",
//                   "lng": "1.4"
//               },
//               {
//                   "lat": "1.3",
//                   "lng": "1.3"
//               },
//               {
//                   "lat": "1.2",
//                   "lng": "1.2"
//               }
//           ]
//       },
//       {
//           "id": 34,
//           "size": "2.77778",
//           "data": "{}",
//           "farmer_id": 2778,
//           "created_at": "2018-08-10T05:34:09.768Z",
//           "landowner": {
//               "id": 34621,
//               "contact_details_id": 2827,
//               "created_at": "2018-08-09T21:26:16.898Z",
//               "date_of_birth": null,
//               "economic_details_id": null,
//               "first_name": "Teeee",
//               "gender": null,
//               "marital_details_id": null,
//               "nationality_id": null,
//               "next_of_kin_id": null,
//               "other_names": "Kwaku",
//               "place_of_birth": null,
//               "surname": "Phlow",
//               "title": null,
//               "age": 23,
//               "region_id": null,
//               "town_id": null,
//               "is_landowner": true,
//               "is_caretaker": true
//           },
//           "caretakers": [
//               {
//                   "id": 34621,
//                   "contact_details_id": 2827,
//                   "created_at": "2018-08-09T21:26:16.898Z",
//                   "date_of_birth": null,
//                   "economic_details_id": null,
//                   "first_name": "Teeee",
//                   "gender": null,
//                   "marital_details_id": null,
//                   "nationality_id": null,
//                   "next_of_kin_id": null,
//                   "other_names": "Kwaku",
//                   "place_of_birth": null,
//                   "surname": "Phlow",
//                   "title": null,
//                   "age": 23,
//                   "region_id": null,
//                   "town_id": null,
//                   "is_landowner": true,
//                   "is_caretaker": true
//               }
//           ],
//           "farm_bounds": [
//               {
//                   "lat": "1.4",
//                   "lng": "1.4"
//               },
//               {
//                   "lat": "1.3",
//                   "lng": "1.3"
//               },
//               {
//                   "lat": "1.2",
//                   "lng": "1.2"
//               }
//           ]
//       },
//       {
//           "id": 37,
//           "size": "2.77778",
//           "data": "{}",
//           "farmer_id": 2778,
//           "created_at": "2018-08-10T05:45:14.757Z",
//           "landowner": {
//               "id": 34841,
//               "contact_details_id": 2851,
//               "created_at": "2018-08-10T05:45:14.811Z",
//               "date_of_birth": null,
//               "economic_details_id": null,
//               "first_name": "George",
//               "gender": null,
//               "marital_details_id": null,
//               "nationality_id": null,
//               "next_of_kin_id": null,
//               "other_names": null,
//               "place_of_birth": null,
//               "surname": "Osae",
//               "title": null,
//               "age": null,
//               "region_id": null,
//               "town_id": 2,
//               "is_landowner": true,
//               "is_caretaker": false
//           },
//           "caretakers": [
//               {
//                   "id": 34851,
//                   "contact_details_id": 2852,
//                   "created_at": "2018-08-10T05:45:14.837Z",
//                   "date_of_birth": null,
//                   "economic_details_id": null,
//                   "first_name": "Kendrick",
//                   "gender": null,
//                   "marital_details_id": null,
//                   "nationality_id": null,
//                   "next_of_kin_id": null,
//                   "other_names": null,
//                   "place_of_birth": null,
//                   "surname": "Lamar",
//                   "title": null,
//                   "age": null,
//                   "region_id": null,
//                   "town_id": 2,
//                   "is_landowner": false,
//                   "is_caretaker": true
//               }
//           ],
//           "farm_bounds": [
//               {
//                   "lat": "1.4",
//                   "lng": "1.4"
//               },
//               {
//                   "lat": "1.3",
//                   "lng": "1.3"
//               },
//               {
//                   "lat": "1.2",
//                   "lng": "1.2"
//               }
//           ]
//       },
//       {
//           "id": 39,
//           "size": "5000.0",
//           "data": "{}",
//           "farmer_id": 2778,
//           "created_at": "2018-08-10T05:46:04.530Z",
//           "landowner": {
//               "id": 34621,
//               "contact_details_id": 2827,
//               "created_at": "2018-08-09T21:26:16.898Z",
//               "date_of_birth": null,
//               "economic_details_id": null,
//               "first_name": "Teeee",
//               "gender": null,
//               "marital_details_id": null,
//               "nationality_id": null,
//               "next_of_kin_id": null,
//               "other_names": "Kwaku",
//               "place_of_birth": null,
//               "surname": "Phlow",
//               "title": null,
//               "age": 23,
//               "region_id": null,
//               "town_id": null,
//               "is_landowner": true,
//               "is_caretaker": true
//           },
//           "caretakers": [
//               {
//                   "id": 34621,
//                   "contact_details_id": 2827,
//                   "created_at": "2018-08-09T21:26:16.898Z",
//                   "date_of_birth": null,
//                   "economic_details_id": null,
//                   "first_name": "Teeee",
//                   "gender": null,
//                   "marital_details_id": null,
//                   "nationality_id": null,
//                   "next_of_kin_id": null,
//                   "other_names": "Kwaku",
//                   "place_of_birth": null,
//                   "surname": "Phlow",
//                   "title": null,
//                   "age": 23,
//                   "region_id": null,
//                   "town_id": null,
//                   "is_landowner": true,
//                   "is_caretaker": true
//               }
//           ],
//           "farm_bounds": [
//               {
//                   "lat": "1.4",
//                   "lng": "1.4"
//               },
//               {
//                   "lat": "1.3",
//                   "lng": "1.3"
//               },
//               {
//                   "lat": "1.2",
//                   "lng": "1.2"
//               }
//           ]
//       }
//   ],
//   "society": {
//       "id": 3,
//       "name": "Guava Farmers Association",
//       "client_id": 4,
//       "district_id": 1,
//       "created_at": "2018-08-08T15:58:54.326Z",
//       "is_active": true,
//       "agent_contract_id": null,
//       "district": "Sector1",
//       "sector": "Sector 2",
//       "farmer_count": 3
//   }
// }