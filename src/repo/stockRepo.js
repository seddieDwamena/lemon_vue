import stockApi from '../api/stockApi.js'

export default {
  get (params) {
    return stockApi.get(params)
  }
 
}
