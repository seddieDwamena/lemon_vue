// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import '@/assets/styles/app.scss';
import AccordionMenu from '@/components/AccordionMenu';
import AccordionMenuItem from '@/components/AccordionMenuItem';
import ActiveUserSettings from '@/components/ActiveUserSettings';
import ActivityComponent from '@/components/ActivityComponent';
import AddEditAgent from '@/components/AddEditAgent';
import AddEditContact from '@/components/AddEditContact';
import AddEditDistrict from '@/components/AddEditDistrict';
import AddEditLoan from '@/components/AddEditLoan';
import AddEditSector from '@/components/AddEditSector';
import AddEditSoceity from '@/components/AddEditSoceity';
import AddEditTown from '@/components/AddEditTown';
import AddLoanApplication from '@/components/AddLoanApplication';
import AddressesFormComp from '@/components/AddressesFormComp.vue';
import AdminBreadCrumb from '@/components/AdminBreadCrumb';
import AdminTable from '@/components/AdminTable';
import AdminUserBreadCrumb from '@/components/AdminUserBreadCrumb';
import AdminUserInfo from '@/components/AdminUserInfo';
import AdminUserSettings from '@/components/AdminUserSettings';
import AdminUsersFilterPane from '@/components/AdminUsersFilterPane';
import AgentProducts from '@/components/AgentProducts';
import AgentService from '@/components/AgentService';
import AgentSettings from '@/components/AgentSettings';
import AgentTable from '@/components/AgentTable';
import AlertBox from '@/components/AlertBox';
import Avatar from '@/components/Avatar';
import BatchTransactions from '@/components/BatchTransactions';
import BottomNav from '@/components/BottomNav';
import BreadCrumbComponent from '@/components/BreadCrumbComponent';
import CheckBox from '@/components/CheckBox';
import ClientSidePanel from '@/components/ClientSidePanel';
import ContactFarmerDetails from '@/components/ContactFarmerDetails';
import ContactSettings from '@/components/ContactSettings';
import ContactView from '@/components/ContactView';
import ConversationComponent from '@/components/ConversationComponent';
import CustomerInfo from '@/components/CustomerInfo';
import DashboardFilter from '@/components/dashboard/DashboardFilter';
import DashboardMap from '@/components/dashboard/DashboardMap';
import DashboardSidePanel from '@/components/dashboard/DashboardSidePanel';
import DashboardSlider from '@/components/dashboard/DashboardSlider';
import FieldTransactionsMap from '@/components/dashboard/FieldTransactionsMap';
import DistrictTable from '@/components/DistrictTable';
import FarmerMap from '@/components/FarmerMap';
//import ReconciledComponent from '@/components/ReconciledComponent'
import FieldTransactions from '@/components/FieldTransactions';
import FileUploader from '@/components/FileUploader.vue';
import FilterBox from '@/components/FilterBox';
import FilterPane from '@/components/FilterPane';
import ArrayInput from '@/components/forms/ArrayInput';
import DateForm from '@/components/forms/DateForm';
import ImageForm from '@/components/forms/ImageForm';
import InputForm from '@/components/forms/InputForm';
import MultiSelect from '@/components/forms/MultiSelect';
import NfInput from '@/components/forms/NfInput';
import NumberForm from '@/components/forms/NumberForm';
import SelectForm from '@/components/forms/SelectForm';
import FullOverlay from '@/components/FullOverlay';
import GroupShow from '@/components/GroupShow';
import HelpComp from '@/components/HelpComp';
import LeftSlidingPanel from '@/components/LeftSlidingPanel';
import CircleLoader from '@/components/loaders/CircleLoader';
import LoadingIndicator from '@/components/LoadingIndicator';
import LoansFilterPane from '@/components/LoansFilterPane';
import MapboxMap from '@/components/MapboxMap';
import MapDiagram from '@/components/MapDiagram';
import MapDisplay from '@/components/MapDisplay';
import MapTransactions from '@/components/MapTransactions';
import NameFilterDropDown from '@/components/NameFilterDropDown';
import OnCheckedMenu from '@/components/OnCheckedMenu';
import RecentTransactions from '@/components/RecentTransactions';
import SearchWithDropdown from '@/components/SearchWithDropdown';
import SectorTable from '@/components/SectorTable';
import SettingsPanel from '@/components/SettingsPanel';
import SingleTransaction from '@/components/SingleTransaction';
import SlideOut from '@/components/SlideOut';
import SliderComp from '@/components/SliderComp';
import SlidingPanel from '@/components/SlidingPanel';
import SoceityTable from '@/components/SoceityTable';
import SwitchButton from '@/components/SwitchButton';
import TabbedViewContacts from '@/components/TabbedViewContacts';
import TableHeader from '@/components/TableHeader';
import TableRow from '@/components/TableRow';
import ToggleComponent from '@/components/ToggleComponent';
import TownTable from '@/components/TownTable';
import TransactionDetails from '@/components/TransactionDetails';
import ReverseTransaction from '@/components/transactions/realtime/ReverseTransaction';
import TransDetail from '@/components/TransDetail';
import TwoPaneContainer from '@/components/TwoPaneContainer';
import Unreconciled from '@/components/Unreconciled';
import UpdateComponent from '@/components/UpdateComponent';
import vSelect from '@/components/vue-select/components/Select.vue';
// import infiniteScroll from 'vue-infinite-scroll'
import infiniteScroll from '@/directives/v-infinite-scroll';
// import fullscreen from 'vue-fullscreen'
// Vue.use(fullscreen)
// localization
import i18n from '@/i18n';
import SectionNode from '@/pages/client/adminsettings/add_edit_user_fields/SectionNode';
import SectionsTree from '@/pages/client/adminsettings/add_edit_user_fields/SectionsTree';
import AddEditGroup from '@/pages/client/adminsettings/users/AddEditGroup';
import AddEditUser from '@/pages/client/adminsettings/users/AddEditUser';
import AllocateFunds from '@/pages/client/adminsettings/users/AllocateFunds';
import EditUser from '@/pages/client/adminsettings/users/EditUser';
import InProgress from '@/pages/InProgress';
import auth from '@/services/auth';
import LocalStorage from '@/services/LocalStorage';
import wsConnection from '@/services/wsConnection';
import { CURRENT_URL } from '@/utils/Constants';
import notify from '@/utils/notify';
import aws from 'aws-sdk';
// element ui
// import ElementUI from 'element-ui'
// import 'element-ui/lib/theme-chalk/index.css'
// Vue.use(ElementUI)
// bootstrap from cdn fails to download at times
// import 'bootstrap/dist/css/bootstrap.min.css'
import axios from 'axios';
import moment from 'moment';
import VTooltip from 'v-tooltip';
import Vue from 'vue';
import VueMoment from 'vue-moment';
import draggable from 'vuedraggable';
import Datepicker from 'vuejs-datepicker';
import Vuelidate from 'vuelidate';
import Vuex from 'vuex';
import App from './App';
import router from './router';
import store from './store';

Vue.use(aws)
Vue.use(require('aws-sdk'))

// customise to prevent conflict with bootstrap tooltip
Vue.use(VTooltip, {
  popover: {defaultBaseClass: 'loans-tooltip loans-popover',
    defaultInnerClass: 'loans-tooltip-inner loans-popover-inner'
  }
})
Vue.filter('notAvailable', function (value) {
  if (!value || value.toString() === '0') return 'N/A'
  value = value.toString()
  return value
})

Vue.component('draggable', draggable)

Vue.component('group-show', GroupShow)

Vue.component('farmer-map', FarmerMap)

Vue.component('contact-farmer-details', ContactFarmerDetails)

Vue.component('datepicker', Datepicker)

Vue.component('sector-table', SectorTable)

Vue.component('district-table', DistrictTable)

Vue.component('add-edit-district', AddEditDistrict)

Vue.component('add-edit-town', AddEditTown)

Vue.component('add-edit-soceity', AddEditSoceity)

Vue.component('add-edit-sector', AddEditSector)

Vue.component('soceity-table', SoceityTable)

Vue.component('town-table', TownTable)

Vue.component('alert-box', AlertBox)

Vue.component('contact-view', ContactView)
Vue.component('single-transaction', SingleTransaction)
Vue.component('client-panel', ClientSidePanel)

Vue.component('help-comp', HelpComp)

Vue.component('circle-loader', CircleLoader)
Vue.component('add-edit-group', AddEditGroup)
Vue.component('add-edit-user', AddEditUser)

Vue.component('edit-user', EditUser)

Vue.component('allocate-funds', AllocateFunds)
Vue.component('add-edit-contact', AddEditContact)
Vue.component('add-edit-agent', AddEditAgent)
Vue.component('add-edit-loan', AddEditLoan)
Vue.component('adminusers-filterpane', AdminUsersFilterPane)

Vue.component('section-node', SectionNode)
Vue.component('sections-tree', SectionsTree)
Vue.component('nf-input', NfInput)

Vue.component('accordion-menu', AccordionMenu)
Vue.component('accordion-menu-item', AccordionMenuItem)

Vue.use(infiniteScroll)

Vue.config.productionTip = false
Vue.use(Vuex)
Vue.use(require('vue-faker'));
Vue.component('table-header', TableHeader)
Vue.component('slider-comp', SliderComp)
Vue.component('table-row', TableRow)
Vue.component('bread-crumb-component', BreadCrumbComponent)
Vue.component('avatar', Avatar)
Vue.component('on-checked-menu', OnCheckedMenu)
Vue.component('tabbed-view-contacts', TabbedViewContacts)
Vue.component('update-component', UpdateComponent)
Vue.component('customer-info', CustomerInfo)
Vue.component('activity-component', ActivityComponent)
Vue.component('conversation-component', ConversationComponent)
Vue.component('name-filter-dropdown', NameFilterDropDown)
Vue.component('search-with-dropdown', SearchWithDropdown)
Vue.component('two-pane-container', TwoPaneContainer)
Vue.component('tabbed-view-contacts', TabbedViewContacts)
Vue.component('recent-transactions', RecentTransactions)
Vue.component('checkbox', CheckBox)
Vue.component('filter-box', FilterBox)
Vue.component('toggle-component', ToggleComponent)
Vue.component('agent-service', AgentService)
Vue.component('agent-products', AgentProducts)
Vue.component('map-transactions', MapTransactions)
Vue.component('map-display', MapDisplay)
Vue.component('map-diagram', MapDiagram)
Vue.component('bottom-nav', BottomNav)
Vue.component('filter-pane', FilterPane)
Vue.component('loans-filter-pane', LoansFilterPane)
Vue.component('agent-table', AgentTable)
Vue.component('sliding-panel', SlidingPanel)
Vue.component('loading-indicator', LoadingIndicator)
Vue.component('settings-panel', SettingsPanel)
Vue.component('in-progress', InProgress)
Vue.component('admin-breadcrumb', AdminBreadCrumb)
Vue.component('admin-user-breadcrumb', AdminUserBreadCrumb)
Vue.component('admin-table', AdminTable)
Vue.component('admin-user-info', AdminUserInfo)
Vue.component('admin-user-settings', AdminUserSettings)
Vue.component('full-overlay', FullOverlay)
Vue.component('add-loan-application', AddLoanApplication)
Vue.component('switch-button', SwitchButton)
Vue.component('un-reconciled', Unreconciled)
//Vue.component('reconciled-component', ReconciledComponent)
Vue.component('field-transactions', FieldTransactions)
Vue.component('batch-transactions', BatchTransactions)
Vue.component('transaction-details', TransactionDetails)
Vue.component('trans-detail', TransDetail)
Vue.component('reverse-transaction', ReverseTransaction)
Vue.component('left-panel', LeftSlidingPanel)
Vue.component('dash-side', DashboardSidePanel)
Vue.component('dash-map', DashboardMap)
Vue.component('trans-map', FieldTransactionsMap)
Vue.component('dash-filter', DashboardFilter)
Vue.component('dash-slider', DashboardSlider)
Vue.component('mapbox-map', MapboxMap)
Vue.component('slide-out', SlideOut)
Vue.component('agent-settings', AgentSettings)
Vue.component('active-user-settings', ActiveUserSettings)
Vue.component('contact-settings', ContactSettings)
Vue.component('input-form', InputForm)
Vue.component('number-form', NumberForm)
Vue.component('date-form', DateForm)
Vue.component('image-form', ImageForm)
Vue.component('select-form', SelectForm)
Vue.component('multi-select', MultiSelect)
Vue.component('array-input', ArrayInput)
Vue.use(VueMoment)
Vue.use(VueMoment)

Vue.component('file-uploader', FileUploader)

Vue.component('v-select', vSelect)

Vue.component('address-form-comp', AddressesFormComp)

Vue.directive('v-infinite-scroll', {
  inserted: function (el) {
    // Focus the element
    el.focus()
  }
})

axios.interceptors.response.use(function (response) {
  return response.data
}, function (error) {
  if (error.response) {
    error = error.response.data
  } else {
    error = {details: 'Error hitting server'}
  }
  return Promise.reject(error)
})

const EventBus = new Vue()

Object.defineProperties(Vue.prototype, {
  $bus: {
    get: function () {
      return EventBus
    }
  },
  $notify: {
    get: function () {
      return notify
    }
  },
  $auth: {
    get: function () {
      return auth
    }
  }
})
if (!Object.byString) {
  Object.byString = function (o, s) {
    s = s.replace(/\[(\w+)\]/g, '.$1') // convert indexes to properties
    s = s.replace(/^\./, '')           // strip a leading dot
    var a = s.split('.')
    for (var i = 0, n = a.length; i < n; ++i) {
      var k = a[i]
      if (k in o) {
        o = o[k]
      } else {
        return
      }
    }
    return o
  }
}
window.requestAnimationFrame = window.requestAnimationFrame ||
    window.mozRequestAnimationFrame ||
    window.webkitRequestAnimationFrame ||
    window.msRequestAnimationFrame ||
    function (f) { return setTimeout(f, 1000 / 60) }
window.cancelAnimationFrame = window.cancelAnimationFrame ||
    window.mozCancelAnimationFrame ||
    function (requestID) { clearTimeout(requestID) }
if (!Array.prototype.take) {
  Array.prototype.take = function (startingIndex, endingIndex) {
    let newArray = []
    for (let i = startingIndex; i < endingIndex; i++) {
      newArray.push(this[i])
    }
    return newArray
  }
}
if (!Object.prototype.forEach) {
Object.defineProperty(Object.prototype, 'forEach', {
  value: function (callback, thisArg) {
    if (this == null) {
      throw new TypeError('Not an object')
    }
    thisArg = thisArg || window
    for (var key in this) {
      if (this.hasOwnProperty(key)) {
        callback.call(thisArg, this[key], key, this);
      }
    }
  }
})
}

Vue.use(Vuelidate)

// if api url changed clear token

let storage = new LocalStorage()
let apiUrl = storage.get('apiUrl', null)
if (apiUrl !== CURRENT_URL) {
  auth.logout()
  storage.set('apiUrl', CURRENT_URL)
}
window.nf = {db: storage, auth, store, router, bus: EventBus, moment, notify, wsConnection}

/* eslint-disable no-new */

new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App },
  i18n
})
