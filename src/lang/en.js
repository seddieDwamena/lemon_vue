export default {
  footer: {
    0: "A Branchless Banking Platform by",
    1: "| KnowledgeBase | Privacy Policy"
  },
  topNav: {
    searchPlaceholder: "Search by Activities, Fonemessenger, Transactions..."
  },
  sideNav: {
    dashboard: "Dashboard",
    fieldTransactions: "Field Transactions",
    selfService: "Self Service",
    identity: "Identity",
    credit: "Credit",
      reports: "Reports",
    settings: "Admin Settings"
  },
  misc: {
    newText: "New",
    balance: "BALANCE",
    active: "active",
    inactive: "inactive",
    offline: "offline",
    successful: "successful",
    pending: "pending",
    failed: "failed"
  },
  dashboard: {
    totalNoAgents: "TOTAL NUMBER OF AGENTS",
    agentType: "agent type",
    agentNetworkDashboard: "Agent Network Dashboard",
    dashboards: "Dashboards"
  },
  contacts: {

  },
  agents: {
    state: "STATE",
    name: "NAME",
    phoneNum: "PRIMARY PHONE NUMBER",
    location:  "LOCATION",
    branch: "BRANCH",
    agentType: "AGENT TYPE",
    supplier: "SUPPLIER",
    status: "STATUS"

  }
}
