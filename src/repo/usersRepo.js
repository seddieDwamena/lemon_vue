import usersApi from '../api/usersApi.js'

export default {
  get (params) {
    return usersApi.get(params)
  },
  // getUsers (params) {
  //   return usersApi.getUsers(params)
  // },
  find (id) {
    return usersApi.find(id) 
  }
}
