import store from '@/store/index'
import axios from 'axios'
import {CURRENT_URL} from '../utils/Constants';

let Auth = function (obj) {
  this.email = ''
  this.password = ''
  this.first_time_login = false
}

let routePrivilegesBag = {
  'contacts': ['c_view_contacts', 'c_delete_contacts', 'c_update_contacts', 
    'c_create_contacts'
  ],
  'contact': [ 'c_view_contacts',
    'c_delete_contacts', 'c_update_contacts', 'c_create_contacts'
  ],
  'agents': ['c_create_agents', 'c_update_agents', 'c_view_agents',
    'c_delete_agents'
  ],
  'agent': ['c_view_agents', 'c_create_agents', 'c_update_agents', 'c_delete_agents']
}

const includesOne = function (haystack, arr) {
  return haystack.some(el => arr.includes(el))
}
window.includesOne = includesOne
window.routePrivilegesBag = routePrivilegesBag

// methods

Auth.prototype.loggedIn = function () {
  if (store.getters.token) {
    return true
  }
  return false
}
Auth.prototype.login = function (email, password) {
  let url = `${CURRENT_URL}client_login`
  let access_token = store.getters.token
  //let url = 'https://d6d17f16.ngrok.io/api/v1/client_login'
  let that = this
  return axios.post(url, {email: email, password: password})
  .then(data => {
    if (data && data.client_id) {
      let clientData = data
      // clientData.first_time_login = true
      that.first_time_login = clientData.first_time_login
      that.email = email 
      // that.password = password 
      store.commit('setClient', clientData)
      return that.getToken(clientData)
    } else {
      return false
    }
  }, error => { return false })
}

Auth.prototype.logout = function () {
  store.commit('setToken', null)
  store.commit('setClient', null)
  store.commit('SET_TOWNS', [])
  return true
}

Auth.prototype.resetPassword = function (password) {
  let url = `${CURRENT_URL}users/change_password`
  //let url = 'https://d6d17f16.ngrok.io/api/v1/client_login'
  let that = this
  return axios.post(url, {new_password: password})
  .then(data => {
    this.first_time_login = false
    return true
    // if(data && data.client_id){
    //   let clientData = data
    //   that.email = email 
    //   that.password = password 
    //   store.commit('setClient', clientData)
    //   return that.getToken(clientData)
    // }else{
    //   return false
    // }
  }, error => { return false })
}
Auth.prototype.checkPrivileges = function (route) {
  let privileges = store.getters.user ? store.getters.user.privileges : []
  let routePrivileges = routePrivilegesBag[route.name]
  if (!routePrivileges) {
    return true
  }
  return includesOne(privileges, routePrivileges)
}

Auth.prototype.getAuthenticatedRoute = function ({to, from}) {
  if (this.checkPrivileges(to)) {
    return to.name
  } else {
    if (from) {
      return from.name
    }
    return 'dashboard'
  }
}

Auth.prototype.getToken = function (clientData) {
  let url = `${CURRENT_URL}token`
  return axios.post(url, {client_id: clientData.client_id,
    client_secret: clientData.client_secret,
    grant_type: 'client_credentials'
  })
  .then(data => {
    if (data.access_token) {
      store.commit('setToken', data.access_token)
      axios.defaults.headers.common['Authorization'] = `Bearer ${data.access_token}`
      axios.defaults.headers.common['Login-Type'] = `client`
      return true
    } else {
      return false
    }
  }, error => {
    return false
  })
}

Auth.prototype.hasPrivilege = function (privs) {
  let privileges = store.getters.user ? store.getters.user.privileges : [] 
  privileges = privileges || []
  return includesOne(privileges, privs)
}

Auth.prototype.canCreateRole = function () {
  return this.hasPrivilege(['c_create_roles', 'c_delete_roles'])
}
Auth.prototype.canEditContact = function () {
  return this.hasPrivilege(['c_create_contacts',
    'c_update_contacts', 'c_delete_contacts'])
}
Auth.prototype.canViewContacts = function () {
  return this.hasPrivilege(['c_create_contacts',
    'c_update_contacts', 'c_delete_contacts', 'c_view_contacts'])
}
Auth.prototype.canEditAgent = function () {
  return this.hasPrivilege(['c_create_agents',
    'c_update_agents', 'c_delete_agents'])
}
Auth.prototype.canViewAgents = function () {
  return this.hasPrivilege(['c_create_agents',
    'c_update_agents', 'c_delete_agents', 'c_view_agents'])
}

const auth = new Auth()

let refreshInterval = 60 * 60 * 40

// setInterval(function(){
//   if(!auth.loggedIn()){
//     console.info('not refreshing')
//     return
//   }
//   console.info('refreshing token')
//   auth.getToken(store.getters.client)
// }, refreshInterval)

// firstFileLoad = false

export default auth
