export const FILTER_DROPDOWN_UPDATED = "filter_dropdown_updated"
export const CONTACT_SEARCH_UPDATED = "contact_search_updated"
export const AGENT_SEARCH_UPDATED = "agent_search_updated"
export const FILTER_BUTTON_CLICKED = "filter_button_clicked"
export const SETTINGS_BUTTON_CLICKED = "settings_button_clicked"
export const SHOW_OVERLAY = "SHOW_OVERLAY"
export const CLOSING_OVERLAY = "CLOSING_OVERLAY"

//admin users table
export const ADD_USER = "ADD_USER"
export const ADD_GROUP = "ADD_GROUP"
export const ALLOCATE_FUNDS = "ALLOCATE_FUNDS"

export const hola = "hola"
