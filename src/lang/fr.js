export default {
  footer: {
    0: "Une plate-forme bancaire sans succursale par",
    1: "| Base de connaissances | Politique de confidentialité"
  },
  misc: {
    newText: "Nouveau",
    balance: "MONTANT",
    active: "actif",
    inactive: "inactif",
    offline: "déconnecté",
    successful: "réussi",
    pending: "en attendant",
    failed: "manqué"
  },
  sideNav: {
    dashboard: "Tableau de Bord",
    fieldTransactions: "Transactions sur le terrain",
    selfService: "En libre service",
    identity: "Identité",
    credit: "Crédit",
    reports: "Rapports",
    settings: "Paramètres d'administration"
  },
  topNav: {
    searchPlaceholder: "Recherche par activités, Fonemessenger, Transactions..."
  },
  dashboard: {
    totalNoAgents: "NOMBRE TOTAL D'AGENTS",
    agentType: "type d'agent",
    agentNetworkDashboard: "Tableau de bord du réseau d'agents",
    dashboards: "Tableaux de bord"
  },
  agents: {
    state: "ÉTAT",
    name: "PRÉNOM",
    phoneNum: "NUMÉRO DE TÉLÉPHONE ENREGISTRÉ",
    location:  "EMPLACEMENT",
    agentType: "TYPE D'AGENT",
    supplier: "FOURNISSEUR",
    status: "STATUT"

  }
}
