import Utils from '@/utils/Utils'

// properties
let AgentsViewModel = function () {

}

// methods
AgentsViewModel.prototype.getForm = function (agent) {
  if (!agent) {
    return {
      service_ids: [],
      first_name: '',
      surname: '',
      other_names: '',
      dob: '',
      place_of_birth: '',
      code: '',
      type: null,
      supplier: 13,
      town_id: null,
      region_id: null,
      // region_id: null,
      active: true,
      addresses: [
        {
          house_number: '',
          street: '',
          description: '',
          city: '',
          suburb: '',
          post_code: ''
        }
      ],
      phone_numbers: [
        {
          carrier: null,
          primary: true,
          ported: false,
          number: ''
        }
      ]
    }
  }
  let form = Utils.cloneObject(agent)
  form.type = agent.agent_type
  form.phone_numbers = [agent.registered_phone]
  if (!agent.registered_phone) {
    form.phone_numbers = [{
      carrier: '',
      primary: true,
      ported: false,
      number: ''
    }]
  } else {
    form.phone_numbers = form.phone_numbers.map(el => {
      return {
        carrier: el.carrier,
        primary: el.primary,
        ported: el.ported,
        number: el.number,
        id: el.id
      }
    })
  }
  if ((!form.addresses && form.addresses[0])){
    form.addresses = [{
      house_number: '',
      street: '',
      description: '',
      city: '',
      suburb: '',
      post_code: ''
    }]
  }
  form.addresses = form.addresses.map(el => {
    return {
      house_number: el.house_number,
      street: el.street,
      description: el.description,
      city: el.city,
      suburb: el.suburb,
      post_code: el.post_code
    }
  })
  return form
}

let agentsViewModel = new AgentsViewModel()
export {agentsViewModel}
