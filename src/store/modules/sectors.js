import sectorsRepo from '../../repo/clientsRepo'
import * as types from '../mutation-types'
import {ASCENDING, CURRENT_URL} from '../../utils/Constants.js'
import Utils from '../../utils/Utils.js'
import axios from 'axios'
import store from '@/store/index'

// initial state
const state = {
  sectors: {
    data: [],
    originalData: null,
    meta: {page: 1},
    errors: [],
    state: 'DATA',
    filters: [],
    sortParams: {param: null, order: ASCENDING},
    sectorCount: 0
  },
  currentsector: null
}

// getters
const getters = {
  sectors: state => state.sectors.data,
  sectorCount: state => state.sectors.data.length,
  // for the viewstate to always show the right view always set the state before the data
  // the viewstate is slightly different from the state
  // the view state is specifically used by the view to render the appropriate screen
  // for eg. a state of loading will have a viewstate of data if there are sectors while  
  // they are loading
  sectorsViewState: state => {
    let sectorsSubModule = state.sectors
    let hasAgents = sectorsSubModule.data && sectorsSubModule.data.length > 0
    if (hasAgents) {
      return 'DATA'
    }
    if (sectorsSubModule.state !== 'LOADING' && sectorsSubModule.state !== 'ERROR') {
      return 'NO-DATA'
    }
    return sectorsSubModule.state
  },
  sectorsMeta: state => state.sectors.meta
}

// actions
const actions = {
  getSectors ({ commit, state, rootState }) {
    let client_code = store.getters.user.client_code
    const URL = `${CURRENT_URL}sectors?client_codes[]=${client_code}&limit=10&page=1`
    axios.get(
      URL
    ).then(r => r)
    .then((sectors) => {
      commit('SET_SECTORS', sectors)
    })
  }
}

// mutations
const mutations = {
  SET_SECTORS (state, sectors) {
    state.sectors.data = sectors.sectors
    state.sectorCount = sectors.sectors.length
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
