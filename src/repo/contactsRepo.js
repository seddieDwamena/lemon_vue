import contactsApi from '../api/contactsApi.js'
//import contactsApi from '../api/mock-api/contactsApi.js'

export default {
  get (params) {
    return contactsApi.get(params)
  },
  findContact (id) {
    //console.log("REPO:",contactsApi.findContact(id))
    return contactsApi.findContact(id)
    
  }
}
