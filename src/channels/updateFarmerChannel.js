import store from '@/store'
import wsConnection from '@/services/wsConnection'
const CHANNEL_NAME = "FarmerUpdatesChannel"

// properties
let UpdateFarmerChannel = function() {
  this.wsConnection = wsConnection
  this.handlers = {}
}


UpdateFarmerChannel.prototype.handleMessage = function (message) {
    if(!message && message.id){
        return
    }

    let handlers = this.handlers[message.id]
    handlers.forEach(handler => {
        handler(message)
    });
}


UpdateFarmerChannel.prototype.subscribe = function (farmerId, handler) {
    this.addHandler(farmerId, handler) 
    let client_code = store.getters.user.client_code
    let identifier =  {channel: CHANNEL_NAME, id: farmerId}
    this.wsConnection.subscribe(`CHANNEL_NAME${farmerId}`, CHANNEL_NAME, identifier, this.handleMessage.bind(this))
  
}

UpdateFarmerChannel.prototype.addHandler = function (farmerId, handler) {
  this.handlers = this.handlers || []
  this.handlers[farmerId] = this.handlers[farmerId] || []
  let handlers = this.handlers[farmerId]
  if(handlers.indexOf(handler) == -1){
    handlers.push(handler)
  }
}

UpdateFarmerChannel.prototype.unsubscribe = function (farmerId, handler) {
    this.handlers = this.handlers || {}
    this.handlers[farmerId] = this.handlers[farmerId] || []
    let handlers = this.handlers[farmerId]
    let index = handlers.indexOf(handler)
    if(index != -1){
        handlers.splice(index, 1)
    }
    if(handlers.length == 0){
        this.wsConnection.unsubscribe(`CHANNEL_NAME${farmerId}`)
    }
  
}


const updateFarmerChannel = new UpdateFarmerChannel()
export default updateFarmerChannel