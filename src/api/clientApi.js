import axios from 'axios'
import {API_URL, FAKE_API_URL, BEIGE_BACKEND, LEMON_DASHBOARD_LOCAL_URL,ngrok_url, CURRENT_URL} from '../utils/Constants.js'
import qs from 'qs'
const USE_FAKE_API = true
import store from '@/store/index'

export default {
  getRegions () {
    const URL = `${CURRENT_URL}/regions`

    return axios.get(
      URL
    )
  }

}
