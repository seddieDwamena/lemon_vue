import axios from 'axios';
import {CURRENT_URL} from '../utils/Constants.js'
import Utils from '../utils/Utils.js'
import Contact from '../models/Contact.js'

export default {
  get (agentId, params) {
    const URL = `${CURRENT_URL}transactions`

    params.page = params.Page || 1

    params.agent_id = agentId
    params.limit = params.PageSize || 30
    // params.state = params.state || "Active"

    return axios.get(URL, {params: params
      // paramsSerializer: function(params) {
      //   return qs.stringify(params, {arrayFormat: 'repeat'})
      // },
    })
    .then(data => {
      let trans = data.transactions.map(el => {
        return el
      })
      let meta = data.meta
      return {
        trans: trans,
        meta: {Page: meta.current_page,
          PageSize: meta.current_page_total,
          TotalPages: meta.total_pages,
          TotalFilteredRecords: meta.total_filtered,
          TotalRecords: meta.total_records
        }
      }
    }) 
  }
}
