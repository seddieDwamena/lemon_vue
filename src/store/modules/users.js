import usersRepo from '../../repo/usersRepo.js'
import * as types from '../mutation-types'
import {ASCENDING, DESCENDING, FIRSTNAME, SURNAME} from '../../utils/Constants.js'
import Utils from '../../utils/Utils.js'

// initial state
const state = {
  users: {
    data: [],
    originalData: null,
    meta: {page: 1},
    errors: [],
    state: 'DATA',
    filters: [],
    sortParams: {param: null, order: ASCENDING},
    userCount: 0
  },
  currentUser: null
}

// getters
const getters = {
  users: state => state.users.data,
  currentUser: state => state.currentAgent,
  usersFilters: state => state.users.filters,
  usersState: state => state.users.state,
  userCount: state => state.users.data.length,
  // for the viewstate to always show the right view always set the state before the data
  // the viewstate is slightly different from the state
  // the view state is specifically used by the view to render the appropriate screen
  // for eg. a state of loading will have a viewstate of data if there are users while  
  // they are loading
  usersViewState: state => {
    let usersSubModule = state.users
    let hasAgents = usersSubModule.data && usersSubModule.data.length > 0
    if (hasAgents) {
      return 'DATA'
    }
    if (usersSubModule.state !== 'LOADING' && usersSubModule.state !== 'ERROR') {
      return 'NO-DATA'
    }
    return usersSubModule.state
  },
  usersMeta: state => state.users.meta
}

// actions
const actions = {
  getUsers ({ commit, state, rootState }) {
    let params = {Page: 1}
    params.access_token = rootState.token
    // params = Utils.buildFiltersQueryParams(params, state.users.filters)
    // params = Utils.buildSortParams(params, state.users.sortParams)
    commit(types.SET_USERS_STATE, 'LOADING')
    return usersRepo.get(params)
    .then((data) => {
      commit(types.RECEIVE_USERS, {users: data.users, originalData: data.users, userCount: data.users.length})
    }, errors => {
      commit(types.RECEIVE_USERS, {errors})
    })
  },
  loadMoreUsers ({commit, state}, page) {
    let params = {Page: page}
    params = Utils.buildFiltersQueryParams(params, state.users.filters)
    params = Utils.buildSortParams(params, state.users.sortParams)
    console.info(params)
    commit(types.SET_USERS_STATE, 'LOADING')
    return usersRepo.get(params)
    .then(data => {
      commit(types.RECEIVE_MORE_USERS, data)
    })
  },
  findUser ({ commit }, id) {
    return new Promise((resolve, reject) => {
      commit(types.CLEAR_CURRENT_USER)
      usersRepo.find(id)
      .then(user => {
        commit(types.RECEIVE_USER, user)
        resolve()
      })
    })
  }
}

// mutations
const mutations = {

  [types.INIT_USERS] (state) {
    // state.users.filters = []
    state.users.sortParams = {param: null, order: ASCENDING}
  },
  /**
   * @param {Object} data - contains users, meta and errors keys
    */
  //  Todo rename to SET_USERS
  [types.RECEIVE_USERS] (state, {users, originalData, meta, errors,userCount}) {
    
    if (errors){
      state.users.state = 'ERROR'
      state.users.errors = errors
    }else{
      state.users.state = 'DATA'
      state.users.data = users
      state.users.originalData = originalData
      state.users.meta = meta
      state.users.userCount = userCount
    }
  },
  [types.SET_USERS_STATE] (state, value) {
    state.users.state = value
  },
  // Todo rename to append users used for pagination
  [types.RECEIVE_MORE_USERS] (state, {users, meta, errors}) {
    state.users.data = state.users.data.concat(users)
    state.users.meta = meta
    state.users.state = 'DATA'
  },
  [types.RECEIVE_USER] (state, user) {
    state.currentAgent = user
  },
  [types.ADD_USERS_FILTER] (state, {name}) {
    let key = Utils.randomString()
    let filter = {key: key, name: name, values: []}
    state.users.filters.push(filter)
  },
  [types.ADD_USERS_FILTER_VALUE] (state, {key, value}) {
    let filter = state.users.filters.find( el => key == el.key)
    filter.values.push(value)
  },
  [types.REMOVE_USERS_FILTER] (state, {key}) {
    let filter = state.users.filters.find( el => key == el.key)
    let index = state.users.filters.indexOf(filter)
    state.users.filters.splice(index, 1)
  },
  [types.REMOVE_USERS_FILTER_VALUE] (state, {key, value}) {
    let filter = state.users.filters.find( el => key == el.key)
    let filterValues = filter.values
    const index = filterValues.indexOf(value);
    filterValues.splice(index, 1);
  },
  [types.CLEAR_USERS_FILTER] (state, {key}) {
    let filter = state.users.filters.find( el => key == el.key)
    filter.values.splice(0)
  },
  [types.CLEAR_CURRENT_USER] (state) {
    state.currentAgent = null
  },
  [types.SET_USER_SORT_PARAMS] (state, sortParams) {
    state.users.sortParams = sortParams
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
