import Vue from 'vue'
import  Utils from '@/utils/Utils'

function getOffset (parentTop, parentOffset, childOffset) {
  return (childOffset - parentOffset) + parentTop
}

function scrollToSegment () {

}

let segmentName
Vue.directive('nf-scrollspy', {
  inserted: function (el, binding) {
    let $el = $(el)

    let $tab = $el.find('.scroll')
    $tab.on('scroll', Utils.debounce(function(e) {
      // console.info('direc', $tab.data('ignore-scroll'))
      if($tab.data('ignore-scroll')){
        return
      }
      let children = $tab.find('[data-form-segment]')
      children.each(function () {
        // console.info('edm', $(this).data('form-segment'))
      })
      let segment
      let parentTop = $tab.scrollTop()
      let parentOffset = $tab.offset().top
      for(let i = 0; i< children.length; i++){
        let child = $(children[i])
        let childOffset = child.offset().top
        let childTop = getOffset(parentTop, parentOffset, childOffset)
        if((parentTop + 60) > getOffset(parentTop, parentOffset, childOffset)){
          segment = child
        }else{
          break
        }
      }
      let oldSegmentName = segmentName 
      segmentName = segment ? segment.data('form-segment') : "Basic Information"
      if(oldSegmentName == segmentName){
        return
      }
      let parent = segment.data('form-segment-parent')
      window.nf.bus.$emit(`nfScrollspy:changed${binding.value.ns}`, {key: segmentName, parent})
    }, 1000))
  }
})

Vue.directive('vuelidate-mod', {
  inserted (el, binding, vnode) {
    alert(binding.value)
    el.addEventListener('change', (event) => {
      let $v = vnode.context.$v
      var $model = Object.byString($v, binding.value)
      console.info($model)
      if($model){
        $model.$touch()
      }
    })
  }
})
