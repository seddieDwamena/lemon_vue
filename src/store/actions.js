import Utils from '@/utils/Utils'

export const getClient = function ({commit}) {
  return new Promise((resolve, reject) => {
    let client = {}
    client.locale = Utils.randomBoolean() ? 'en-gb' : 'fr'
    commit('getClient', client)
    resolve(client)
  })
}
